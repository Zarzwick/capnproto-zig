@0x8308d794342849a9 ;

struct StructWithDefaults {
	x @0 :Float32 = 0.5 ;
	y @1 :Float32 = 1.0 ;
	z @2 :Float64 = 1e9 ;
}

struct Root {
	idioticCoords @0 :List(StructWithDefaults) ;
}
