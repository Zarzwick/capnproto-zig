@0xd65ae7d3f9df4444 ;

struct Outer {
	union {
		foo @0 : Int32 ;
		bar @1 : Int64 ;
		baz @2 : Int8 ;
	}
	andNowForSomethingDifferent :union {
		foo @3 : Int32 ;
		bar @4 : Int64 ;
		baz @5 : Bool ;
	}
}
