const std   = @import("std") ;
const capnp = @import("capnp") ;

// The various test sets.
const basic         = @import("basic.zig") ;
const primitives    = @import("primitives.zig") ;
const blobs         = @import("blobs.zig") ;
const lists         = @import("lists.zig") ;
const defaults      = @import("defaults.zig") ;
const unions        = @import("unions.zig") ;
const los           = @import("list_of_structs.zig") ;

test "basic" {
    // Get an allocator, and create a message buffer.
    var message = try capnp.MessageBuffer.init(1024, std.heap.c_allocator) ;
    message.heuristic = capnp.serialization.Heuristic.nostorage ;
    // Fill the message with values corresponding to the JSON.
    var a = try basic.A.Stencil.initAsRoot(&message) ;
    try a.setE(3) ;
    // Check the getter as well.
    const e_check = try a.getE() ;
    try std.testing.expectEqual(e_check, 3) ;
    // Write to a file.
    const ref_filename = "basic,ref.bin" ;
    const ours_filename = "basic,ours.bin" ;
    const cwd = std.fs.cwd() ;
    var file = try cwd.createFile(ours_filename, .{}) ;
    var stream = std.io.StreamSource { .file = file } ;
    try message.feedTo(stream.writer()) ;
    // Close file and check the reference matches our version.
    message.deinit() ;
    std.fs.File.close(file) ;
    // Compare file contents.
    try std.testing.expect(try filesAreEqual(ref_filename, ours_filename)) ;
}

test "writing, structs, primitives" {
    // Get an allocator, and create a message buffer.
    var message = try capnp.MessageBuffer.init(1024, std.heap.c_allocator) ;
    message.heuristic = capnp.serialization.Heuristic.nostorage ;
    // Fill the message with values corresponding to the JSON.
    var foo = try primitives.Foo.Stencil.initAsRoot(&message) ;
    try foo.setD(12) ;
    try foo.setE0(true) ;
    try foo.setE1(false) ;
    try foo.setE2(true) ;
    try foo.setE3(false) ;
    try foo.setE4(true) ;
    try foo.setE5(false) ;
    try foo.setG(primitives.Truc.chouette) ;
    var bar = try foo.initBar() ;
    try bar.setA(334209) ;
    try bar.setB(78) ;
    try bar.setC(5.0) ;
    var nested0 = try bar.initNested0() ;
    try nested0.setF(9430) ;
    var nested1 = try bar.initNested1() ;
    try nested1.setF(342) ;
    // Write to a file.
    const ref_filename = "primitives,ref.bin" ;
    const ours_filename = "primitives,ours.bin" ;
    const cwd = std.fs.cwd() ;
    var file = try cwd.createFile(ours_filename, .{}) ;
    var stream = std.io.StreamSource { .file = file } ;
    try message.feedTo(stream.writer()) ;
    // Close file and check the reference matches our version.
    message.deinit() ;
    std.fs.File.close(file) ;
    // Compare file contents.
    try std.testing.expect(try filesAreEqual(ref_filename, ours_filename)) ;
}

test "writing, list of structs" {
    // Get an allocator, and create a message buffer.
    var message = try capnp.MessageBuffer.init(1024, std.heap.c_allocator) ;
    // Fill the message with values corresponding to the JSON.
    var root = try los.Outer.Stencil.initAsRoot(&message) ;
    var inners = try root.initInners(2) ;
    var inner_0 = try inners.get(0) ;
    var inner_1 = try inners.get(1) ;
    try inner_0.setA(72) ;
    try inner_0.setB(8) ;
    try inner_1.setA(107) ;
    try inner_1.setB(7) ;
    // Write to a file.
    const ref_filename  = "list_of_structs,ref.bin" ;
    const ours_filename = "list_of_structs,ours.bin" ;
    const cwd = std.fs.cwd() ;
    var file = try cwd.createFile(ours_filename, .{}) ;
    var stream = std.io.StreamSource { .file = file } ;
    try message.feedTo(stream.writer()) ;
    // Close file and check the reference matches our version.
    message.deinit() ;
    std.fs.File.close(file) ;
    // Compare file contents.
    try std.testing.expect(try filesAreEqual(ref_filename, ours_filename)) ;
}

test "writing, default values" {
    // Get an allocator, and create a message buffer.
    var message = try capnp.MessageBuffer.init(1024, std.heap.c_allocator) ;
    // Fill the message with values corresponding to the JSON.
    var root = try defaults.Root.Stencil.initAsRoot(&message) ;
    var coords = try root.initIdioticCoords(4) ;
    // And now because of the default values, this should be ok.
    // We can also check for reader function.
    const coord3 = try coords.get(3) ;
    const coord3x = try coord3.getX() ;
    const coord3y = try coord3.getY() ;
    const coord3z = try coord3.getZ() ;
    try std.testing.expect(coord3x == 0.5) ;
    try std.testing.expect(coord3y == 1.0) ;
    try std.testing.expect(coord3z == 1e9) ;
    // Write to a file.
    const ref_filename  = "defaults,ref.bin" ;
    const ours_filename = "defaults,ours.bin" ;
    const cwd = std.fs.cwd() ;
    var file = try cwd.createFile(ours_filename, .{}) ;
    var stream = std.io.StreamSource { .file = file } ;
    try message.feedTo(stream.writer()) ;
    // Close file and check the reference matches our version.
    message.deinit() ;
    std.fs.File.close(file) ;
    // Compare file contents.
    try std.testing.expect(try filesAreEqual(ref_filename, ours_filename)) ;
}

// test "writing, lists" {
//     // Get an allocator, and create a message buffer.
//     var message = try capnp.MessageBuffer.init(1024, std.heap.c_allocator) ;
//     // Fill the message with values corresponding to the JSON.
//     var root = try lists.ListOfListOfList.Stencil.initAsRoot(&message) ;
//     var list_of_list_of_list = try root.initListOfListOfList(3) ;
//     var list_of_list_struct_0 = try list_of_list_of_list.get(0) ;
//     //var list_of_list_struct_1 = try list_of_list_of_list.get(1) ;
//     //var list_of_list_struct_2 = try list_of_list_of_list.get(2) ;
//     var list_of_list_0 = try list_of_list_struct_0.initListOfList(2) ;
//     var list_of_list_1 = try list_of_list_struct_0.initListOfList(3) ;
//     var list_of_list_2 = try list_of_list_struct_0.initListOfList(1) ;
//     _ = list_of_list_0 ;
//     _ = list_of_list_1 ;
//     _ = list_of_list_2 ;
//     //var list_0_0 = try list_of_list_0.initSub(0, 0) ;
//     //var list_0_1 = try list_of_list_0.initSub(1, 6) ;
//     //var list_1_0 = try list_of_list_0.initSub(0, 4) ;
//     //var list_1_1 = try list_of_list_0.initSub(1, 0) ;
//     //var list_1_2 = try list_of_list_0.initSub(2, 3) ;
//     //var list_2_0 = try list_of_list_0.initSub(0, 4) ;
//     //try list_0_1.set(0, 12) ;
//     //try list_0_1.set(0, 24) ;
//     //try list_0_1.set(0, 36) ;
//     //try list_0_1.set(0, 25) ;
//     //try list_0_1.set(0, 11) ;
//     //try list_0_1.set(0,  9) ;
//     //try list_1_0.set(0,  1) ;
//     //try list_1_0.set(1,  2) ;
//     //try list_1_0.set(2,  3) ;
//     //try list_1_0.set(3, 12) ;
//     //try list_1_2.set(0, 21) ;
//     //try list_1_2.set(0, 23) ;
//     //try list_1_2.set(0,  0) ;
//     //try list_2_0.set(0,  3) ;
//     //try list_2_0.set(1,  3) ;
//     //try list_2_0.set(2,  3) ;
//     //try list_2_0.set(3, 1209) ;
//     try std.testing.expect(true) ;
// }

test "writing, blobs" {
    // Get an allocator, and create a message buffer.
    var message = try capnp.MessageBuffer.init(1024, std.heap.c_allocator) ;
    message.heuristic = capnp.serialization.Heuristic.nostorage ;
    // Fill the message with values corresponding to the JSON.
    const name = "我听不懂 que dalle 😂" ++ [1]u8 {0} ;
    const data = [_]u8 {0, 255, 0, 255, 0, 255, 127, 127} ;
    var twoblobs = try blobs.TwoBlobsOneJar.Stencil.initAsRoot(&message) ;
    _ = try twoblobs.initNameWith(name) ;
    _ = try twoblobs.initDataWith(data[0..]) ;
    // Check getters.
    const name_stencil = try twoblobs.getName() ;
    const data_stencil = try twoblobs.getData() ;
    var name_check: [name.len]u8 = undefined ;
    var data_check: [data.len]u8 = undefined ;
    try name_stencil.getSlice(0, &name_check) ;
    try data_stencil.getSlice(0, &data_check) ;
    std.debug.print("\nName: {s}\nData: ", .{name_check}) ;
    // for (name_check) |c| {
    //     std.debug.print("{}, ", .{c}) ;
    // }
    std.debug.print("\n", .{}) ;
    std.debug.print("└─ The above is *not* working properly yet.", .{}) ;
    //try std.testing.expect(std.mem.eql(u8,  name, &name_check)) ;
    //try std.testing.expect(std.mem.eql(u8, &data, &data_check)) ;
    // Write to a file.
    const ref_filename = "blobs,ref.bin" ;
    const ours_filename = "blobs,ours.bin" ;
    const cwd = std.fs.cwd() ;
    var file = try cwd.createFile(ours_filename, .{}) ;
    var stream = std.io.StreamSource { .file = file } ;
    try message.feedTo(stream.writer()) ;
    // Close file and check the reference matches our version.
    message.deinit() ;
    std.fs.File.close(file) ;
    // Compare converted JSON.
    try std.testing.expect(try jsonEquivalentsAreEqual(ref_filename, ours_filename, "blobs.capnp", "TwoBlobsOneJar")) ;
}

test "writing, union and groups" {
    // Get an allocator, and create a message buffer.
    var message = try capnp.MessageBuffer.init(1024, std.heap.c_allocator) ;
    message.heuristic = capnp.serialization.Heuristic.nostorage ;
    // Fill the message with values corresponding to the JSON.
    var outer = try unions.Outer.Stencil.initAsRoot(&message) ;
    try outer.setBar(10) ;
    var something = outer.getAndNowForSomethingDifferent() ;
    try something.setBaz(true) ;
    // Write to a file.
    const ref_filename = "unions,ref.bin" ;
    const ours_filename = "unions,ours.bin" ;
    const cwd = std.fs.cwd() ;
    var file = try cwd.createFile(ours_filename, .{}) ;
    var stream = std.io.StreamSource { .file = file } ;
    try message.feedTo(stream.writer()) ;
    // Close file and check the reference matches our version.
    message.deinit() ;
    std.fs.File.close(file) ;
    // Compare file contents.
    try std.testing.expect(try filesAreEqual(ref_filename, ours_filename)) ;
}

//test "reading, primitives" {
//    pub fn main() !void {
//        // Open file.
//        const cwd = std.fs.cwd() ;
//        var file = try cwd.openFile("primitivesample,ours.bin", .{}) ;
//        defer std.fs.File.close(file) ;
//        // Get a buffered reader.
//        //var reader = std.io.bufferedReader(file.reader()).reader() ;
//        var stream = std.io.StreamSource { .file = file } ;
//        // Observe !
//        var message = try capnp.MessageStream.init(&stream) ;
//        const view = try prim.Bar.View.init(&message) ;
//        std.debug.print("{?}\n", .{view}) ;
//        std.debug.print("{}\n",  .{view.getA(&message)}) ;
//        //std.debug.print("{?}\n", .{view.getB(&message)}) ;
//        std.debug.print("{}\n",  .{view.getC(&message)}) ;
//        //const list = try view.getB(&message) ;
//        //var i : u64 = 0 ;
//        //while (i < list.size) : (i += 1) {
//        //    std.debug.print("{}\n", .{list.get(u16, i, &message)}) ;
//        //}
//    }
//}

test "compression" {
    const input = [_]u8
        { 0x08, 0x00, 0x00, 0x00, 0x03, 0x00, 0x02, 0x00
        , 0x19, 0x00, 0x00, 0x00, 0xAA, 0x01, 0x00, 0x00
        } ;
    const expected = [_]u8
        { 0x51, 0x08, 0x03, 0x02, 0x31, 0x19, 0xAA, 0x01
        } ;
    var output : [input.len]u8 = undefined ;
    // Make an encoder on top of a fixed buffer writer.
    var fixed = std.io.FixedBufferStream([]u8) { .buffer = output[0..], .pos = 0 } ;
    var fixed_writer = fixed.writer() ;
    var compressor = capnp.Compressor(@TypeOf(fixed_writer))
        { .underlying_writer = fixed_writer
        , .total_bytes_written = 0
        , .bytes_written = 0
        } ;
    var writer = compressor.writer() ;
    try writer.writeAll(input[0..]) ;
    //std.debug.print("\n", .{}) ;
    //std.debug.print("Expect : {any}\n", .{expected}) ;
    //std.debug.print("Output : {any}\n", .{output[0..encoder.total_bytes_written]}) ;
    // Check.
    try std.testing.expect(
        std.mem.eql(u8, expected[0..], output[0..compressor.total_bytes_written])
    ) ;
}

test "compression (unpacked, temporary)" {
    var input: [32]u8 = undefined ;
    for (input) |*b| {
        b.* = 0x8a ;
    }
    var expected: [34]u8 = undefined ;
    for (expected) |*b| {
        b.* = 0x8a ;
    }
    expected[0] = 0xff ;
    expected[9] = 0x03 ;
    var output: [expected.len]u8 = undefined ;
    // Make an encoder on top of a fixed buffer writer.
    var fixed = std.io.FixedBufferStream([]u8) { .buffer = output[0..], .pos = 0 } ;
    var fixed_writer = fixed.writer() ;
    var compressor = capnp.Compressor(@TypeOf(fixed_writer))
        { .underlying_writer = fixed_writer
        , .total_bytes_written = 0
        , .bytes_written = 0
        } ;
    try compressor.compressUnpackedRegion(&input) ;
    //var writer = compressor.writer() ;
    //try writer.writeAll(input[0..]) ;
    std.debug.print("\n", .{}) ;
    std.debug.print("Expect : {any}\n", .{expected}) ;
    std.debug.print("Output : {any}\n", .{output[0..compressor.total_bytes_written]}) ;
    // Check.
    try std.testing.expect(
        std.mem.eql(u8, expected[0..], output[0..compressor.total_bytes_written])
    ) ;
}

test "decompression" {
    const input = [_]u8
        { 0x51, 0x08, 0x03, 0x02, 0x31, 0x19, 0xAA, 0x01
        } ;
    const expected = [_]u8
        { 0x08, 0x00, 0x00, 0x00, 0x03, 0x00, 0x02, 0x00
        , 0x19, 0x00, 0x00, 0x00, 0xAA, 0x01, 0x00, 0x00
        } ;
    var output : [expected.len+25]u8 = undefined ;
    // Make an encoder on top of a fixed buffer writer.
    var fixed = std.io.FixedBufferStream([]const u8) { .buffer = input[0..], .pos = 0 } ;
    var fixed_reader = fixed.reader() ;
    //_ = try fixed_reader.readAll(output[0..input.len]) ;
    var decompressor = capnp.Decompressor(@TypeOf(fixed_reader))
        { .underlying_reader = fixed_reader
        , .total_bytes_read = 0
        , .bytes_read = 0
        } ;
    var reader = decompressor.reader() ;
    const decompressed_bytes_count = try reader.read(output[0..]) ;
    //std.debug.print("\n", .{}) ;
    //std.debug.print("Decoded {} bytes for an output of {} bytes.\n", .{decompressed_bytes_count, output.len}) ;
    //std.debug.print("Expect : {any}\n", .{expected}) ;
    //std.debug.print("Output : {any}\n", .{output[0..decompressed_bytes_count]}) ;
    ////std.debug.print("Output : {any}\n", .{output[0..decoder.total_bytes_read]}) ;
    // Check.
    try std.testing.expect(
        std.mem.eql(u8, expected[0..], output[0..decompressed_bytes_count])
    ) ;
}

///
/// Opens and load two files into memory, and compare.
/// TODO This could probably be prettier.
///
pub fn filesAreEqual(path_a: []const u8, path_b: []const u8) !bool {
    const buffer_size = 1024 * 16 ; // 16 kib.
    var buffer_a : [buffer_size]u8 = undefined ;
    var buffer_b : [buffer_size]u8 = undefined ;
    const cwd = std.fs.cwd() ;
    var file_a = try cwd.openFile(path_a, .{}) ;
    var file_b = try cwd.openFile(path_b, .{}) ;
    defer file_a.close() ;
    defer file_b.close() ;
    var stream_a = std.io.StreamSource { .file = file_a } ;
    var stream_b = std.io.StreamSource { .file = file_b } ;
    var reader_a = stream_a.reader() ;
    var reader_b = stream_b.reader() ;
    var valid = true ;
    while (valid) {
        const bytes_read_a = try reader_a.read(buffer_a[0..]) ;
        const bytes_read_b = try reader_b.read(buffer_b[0..]) ;
        if (bytes_read_a != bytes_read_b) {
            valid = false ;
            break ;
        }
        if (bytes_read_a == 0) {
            break ;
        }
        valid = std.mem.eql(u8, buffer_a[0..bytes_read_a], buffer_b[0..bytes_read_b]) ;
    }
    return valid ;
}

///
/// Obviously this is completely non-portable. Btw I wonder how
/// people on windows use capnp. Do they have a GUI for that too ?
///
pub fn jsonEquivalentsAreEqual(
    path_a: []const u8,
    path_b: []const u8,
    path_capnp: []const u8,
    root_capnp: []const u8
) !bool {
    const args = [_][]const u8
        { "sh", "jsonify.sh"
        , path_a
        , path_b
        , path_capnp
        , root_capnp
        } ;
    var proc = std.ChildProcess.init(&args, std.heap.c_allocator) ;
    _ = try proc.spawnAndWait() ;
    return filesAreEqual("test,a.json", "test,b.json") ;
}
