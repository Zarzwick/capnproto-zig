.PHONY: generation
.PHONY: plugin
.PHONY: tests
.PHONY: poc

ZIG = zig

all: plugin generation tests

plugin:
	mkdir -p build
	cd build ; \
	meson setup .. ; \
	ninja

generation: plugin
# 	Generate zig code for our examples. I redirect to /dev/null for the
# 	purpose of the script. When actually debugging I have it printed on
# 	the console.
	cd build ; \
	capnpc ../tests/basic.capnp -o-      | ./capnpc-zig-alt > ../tests/basic.zig        2>&1 ; \
	capnpc ../tests/primitives.capnp -o- | ./capnpc-zig-alt > ../tests/primitives.zig   2>&1 ; \
	capnpc ../tests/blobs.capnp -o-      | ./capnpc-zig-alt > ../tests/blobs.zig        2>&1 ; \
	capnpc ../tests/lists.capnp -o-      | ./capnpc-zig-alt > ../tests/lists.zig        2>&1 ; \
	capnpc ../tests/defaults.capnp -o-   | ./capnpc-zig-alt > ../tests/defaults.zig     2>&1 ; \
	capnpc ../tests/unions.capnp -o-     | ./capnpc-zig-alt > ../tests/unions.zig       2>&1 ; \
	capnpc ../tests/list_of_structs.capnp -o- | ./capnpc-zig-alt > ../tests/list_of_structs.zig 2>&1
# 	Create reference binaries to compare to.
	cd tests ; \
	cat basic.json       | capnp convert json:binary basic.capnp A                > basic,ref.bin ; \
	cat primitives.json  | capnp convert json:binary primitives.capnp Foo         > primitives,ref.bin ; \
	cat blobs.json       | capnp convert json:binary blobs.capnp TwoBlobsOneJar   > blobs,ref.bin ; \
	cat lists.json       | capnp convert json:binary lists.capnp ListOfListOfList > lists,ref.bin ; \
	cat defaults.json    | capnp convert json:binary defaults.capnp Root          > defaults,ref.bin ; \
	cat unions.json      | capnp convert json:binary unions.capnp Outer           > unions,ref.bin ; \
	cat list_of_structs.json | capnp convert json:binary list_of_structs.capnp Outer > list_of_structs,ref.bin

tests: generation
	cd tests ; \
	$(ZIG) test tests.zig -lc --pkg-begin capnp '../src/capnp.zig' --pkg-end --test-filter 'basic' ; \
	$(ZIG) test tests.zig -lc --pkg-begin capnp '../src/capnp.zig' --pkg-end --test-filter 'default' ; \
	$(ZIG) test tests.zig -lc --pkg-begin capnp '../src/capnp.zig' --pkg-end --test-filter 'blobs'
#	$(ZIG) test tests.zig -lc --pkg-begin capnp '../src/capnp.zig' --pkg-end --test-filter 'primitives'
#	$(ZIG) test tests.zig -lc --pkg-begin capnp '../src/capnp.zig' --pkg-end --test-filter 'list of structs'
# 	# At some point, I'll just collapse all those lines above.
# 	cd src ; \
#   $(ZIG) test capnp.zig -lc

poc:
	cd src/plugin ; \
	ghc poc.hs -dynamic -o '../../poc'

clean:
	rm -f tests/*.bin
	rm -f tests/basic.zig
	rm -f tests/primitives.zig
	rm -f tests/list_of_structs.zig
	rm -f tests/lists.zig
	rm -f tests/unions.zig
	rm -f tests/test,a.json tests/test,b.json
	rm -f poc
	cd build ; ninja clean
