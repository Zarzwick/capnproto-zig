const std = @import("std") ;

pub const base              = @import("capnp/base.zig") ;
pub const serialization     = @import("capnp/serialization.zig") ;
//pub const deserialization   = @import("capnp/deserialization.zig") ;
pub const compression       = @import("capnp/compression.zig") ;
pub const decompression     = @import("capnp/decompression.zig") ;

pub const bytes_per_word = base.bytes_per_word ;

pub const MessageBuffer = serialization.MessageBuffer ;

pub const Compressor    = compression.Compressor ;
pub const Decompressor  = decompression.Decompressor ;

test {
    std.testing.refAllDecls(@This()) ;
}
