const std  = @import("std") ;
const base = @import("base.zig") ;

const bytes_per_word = base.bytes_per_word ;

pub const Error = error { OutOfBounds } ;

///
/// Information acquired from the beginning of the stream, regarding framing
/// and segmentation.
///
pub const MessageStream = struct {
    const max_segments: usize = 16 ;
    segments_count: usize, // TODO, REMOVE
    segments_sizes: [max_segments]usize, //std.ArrayList(usize), // TODO, ALLOCATION
    first_segment_address: usize, // in words
    stream: *std.io.StreamSource,

    pub fn init(stream: *std.io.StreamSource) !MessageStream {
        // Framing info. takes at least 1 word, so read it.
        var info: MessageStream = undefined ;
        var reader = stream.reader() ;
        // Read number of segments.
        var bytes = try reader.readBytesNoEof(4) ;
        info.segments_count = std.mem.readIntLittle(u32, &bytes) + 1 ;
        // Read sizes.
        for (info.segments_sizes[0..info.segments_count]) |*size| {
            bytes = try reader.readBytesNoEof(4) ;
            size.* = std.mem.readIntLittle(u32, &bytes) ;
        }
        // Consume padding, if any.
        if ((info.segments_count & 1) == 0) {
            bytes = try reader.readBytesNoEof(4) ;
        }
        info.stream = stream ;
        return info ;
    }
} ;


///
/// ...
///
pub const Struct = struct {
    pub const View = struct {
        content_pos: u64,
        data_section_size: u64,
        ptr_section_size: u64,
        const Self = @This() ;
        pub fn init(stream: *std.io.StreamSource) !Self {
            const bytes = try stream.reader().readBytesNoEof(bytes_per_word) ;
            const word = std.mem.readIntLittle(u64, &bytes) ;
            //std.debug.print("{any} {}\n", .{bytes, word})  ;
            if (word & base.Masks.object_type != 0) {
                std.debug.print("It is not a struct.\n", .{}) ;
            }
            const pos = try stream.getPos() ;
            return Self
                { .content_pos = (pos / bytes_per_word) +
                    ((word & base.Masks.struct_offset) >> 2)
                , .data_section_size = (word & base.Masks.struct_datasec) >> 32
                , .ptr_section_size = (word & base.Masks.struct_ptrsec) >> 48
                } ;
        }
    } ;
} ;


pub const List = struct {
    pub const View = struct {
        content_pos: u64,
        bits_per_element: u64,
        size: u64,
    
        const Self = @This() ;
    
        pub fn init(stream: *std.io.StreamSource) !Self {
            const bytes = try stream.reader().readBytesNoEof(bytes_per_word) ;
            const word = std.mem.readIntLittle(u64, &bytes) ;
            //std.debug.print("{any} {}\n", .{bytes, word})  ;
            if (word & base.Masks.object_type != 1) {
                std.debug.print("It is not a list.\n", .{}) ;
            }
            const pos = try stream.getPos() ;
            return Self
                { .content_pos = (pos / bytes_per_word) + ((word & base.Masks.list_offset) >> 2)
                , .bits_per_element = base.interpretSize((word & base.Masks.list_elem_size) >> 32)
                , .size = (word & base.Masks.list_size) >> 35
                } ;
        }

        pub fn get(self: Self, comptime T: type, i: u64, message: *MessageStream) !T {
            // TODO Find a better way for T.
            if (i >= self.size) {
                return Error.OutOfBounds ;
            }
            const bpe = self.bits_per_element ;
            const bytes_per_element = if (bpe == 1) (bpe % 8) else (bpe / 8) ;
            const pos = (self.content_pos * bytes_per_word) + bytes_per_element * i ;
            try message.*.stream.*.seekTo(pos) ;
            var buffer : [8]u8 = undefined ;
            var bytes = buffer[0..bytes_per_element] ;
            try message.*.stream.reader().readNoEof(bytes) ;
            return std.mem.readIntLittle(T, bytes[0..@sizeOf(T)]) ;
        }
    } ;
} ;
    

///
/// This function will check bounds and read the various types from the
/// stream. All the generated getters will in fact call this function.
///
/// This function should not be used by the user. You can, but it is not
/// meant for it and you are likely to read trash without a reliable
/// position.
///
pub fn getInternal(
    comptime T: type,
    stream: *std.io.StreamSource,
    pos: u64
) !T {
    // TODO Check bounds :|
    switch (T) {
        Struct.View => {
            try stream.seekTo(pos) ;
            return try StructView.init(stream) ;
        },
        List.View => {
            try stream.seekTo(pos) ;
            return try ListView.init(stream) ;
        },
        f32 => {
            try stream.seekTo(pos) ;
            const bytes = try stream.reader().readBytesNoEof(@sizeOf(T)) ;
            const int = std.mem.readIntLittle(u32, &bytes) ;
            return @bitCast(T, int) ;
        },
        f64 => {
            try stream.seekTo(pos) ;
            const bytes = try stream.reader().readBytesNoEof(@sizeOf(T)) ;
            const int = std.mem.readIntLittle(u64, &bytes) ;
            return @bitCast(T, int) ;
        },
        bool, i8, i16, i32, i64, u8, u16, u32, u64 => {
            try stream.seekTo(pos) ;
            const bytes = try stream.reader().readBytesNoEof(@sizeOf(T)) ;
            return std.mem.readIntLittle(T, &bytes) ;
        },
        else => unreachable,
    }
}

// ///
// /// Returns the view type associated with a given type.
// ///
// pub fn View(comptime T: type) type {
    // return switch (T) {
        // bool, i8, u8, i16, u16, i32, u32, i64, u64 => T,
        // else => T.View
    // } ;
// }
