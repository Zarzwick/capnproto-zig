const std       = @import("std") ;
const builtin   = @import("builtin") ;
const base      = @import("base.zig") ;

const Allocator = std.mem.Allocator ;
const ArrayList = std.ArrayList ;

pub const Error = error {
    ListTooLong,
    OutOfBounds,
    EmptySegment,
    TooLargeForSegment,
    BadGetMethodForNestedList,
} ;

const bytes_per_word = base.bytes_per_word ;

pub const max_list_len = (0x1 << 30) - 1 ;


///
/// Returns the offset between to pointers taken from slices.
///
fn offsetBetween(a: []const u8, b: []const u8) u32 {
    const addr_a = @ptrToInt(&a[0]) ;
    const addr_b = @ptrToInt(&b[0]) ;
    const addr_max = @max(addr_a, addr_b) ;
    const addr_min = @min(addr_a, addr_b) ;
    const offset = addr_max - addr_min ;
    if (offset > ((1<<32) - 1)) { unreachable ; }
    return @intCast(u32, offset) ;
}

///
/// Describes the nature of elements in a list.
///
const ListElementType = enum { Normal, Pointer, Composite, Bool } ;

///
/// Returns the proper element descriptor to write in field C. Sad story,
/// I had to force myself not thinking to tabulate this function... CG scars.
///
fn encodeListElementSize(size: u32, etype: ListElementType) u32 {
    return switch (etype) {
        ListElementType.Bool => 1,
        ListElementType.Normal => encodeNormalListElementSize(size),
        ListElementType.Pointer => 6,
        ListElementType.Composite => 7,
    } ;
}

fn encodeNormalListElementSize(size: u32) u32 {
    return switch (size) {
        1 => 2,
        2 => 3,
        4 => 4,
        8 => 5,
        // Should be 6 for a pointer.
        else => unreachable
    } ;
}

fn decodeNormalListElementSize(code: u32) u32 {
    return switch (code) {
        2 => 1,
        3 => 2,
        4 => 4,
        5 => 8,
        6 => 8,
        else => unreachable
    } ;
}

///
/// The size of elements must be either 1 or a multiple of 8.
///
fn encodeListPointer(
    offset: u32, // in bytes.
    list_size: u32,
    list_elem_size: u32, // In bytes, ignored if type is Bool.
    list_elem_type: ListElementType,
    bytes: *[bytes_per_word]u8,
) void {
    const elem_size_code = encodeListElementSize(list_elem_size, list_elem_type) ;
    const len_or_words_count = if (list_elem_type == ListElementType.Composite)
        list_size * list_elem_size / bytes_per_word else list_size ;
    var word : u64 = 0 ;
    word |= base.Masks.object_type    & (@intCast(usize, 0x1)) ;
    word |= base.Masks.list_offset    & (@intCast(usize, offset / bytes_per_word) << 2) ;
    word |= base.Masks.list_elem_size & (@intCast(usize, elem_size_code) << 32) ;
    word |= base.Masks.list_size      & (@intCast(usize, len_or_words_count) << 35) ;
    std.mem.writeIntLittle(u64, bytes, word) ;
}

///
/// Enccode a pointer to a struct in the provided slice.
///
pub fn encodeStructPointer(
    data_offset: u32,
    data_size: u32,
    ptr_size: u32,
    bytes: *[bytes_per_word]u8,
) void {
    var word : u64 = 0 ;
    word |= base.Masks.struct_offset  & (@intCast(usize, data_offset) << 2) ;
    word |= base.Masks.struct_datasec & (@intCast(usize, data_size) << 32) ;
    word |= base.Masks.struct_ptrsec  & (@intCast(usize, ptr_size) << 48) ;
    std.mem.writeIntLittle(u64, bytes, word) ;
}

///
/// Encode a far pointer in the provided bytes. If indirection is true, this
/// is the "B = 1" case of the cap'n'proto documention, not supported yet.
///
fn encodeFarPointer(
    landing_pad_seg: u32,
    landing_pad_off: u32,
    bytes: *[bytes_per_word]u8,
    indirection: bool,
) void {
    // TODO Check that the offset fits in 29 bits...
    // TODO Handle the (rare) indirection case.
    if (indirection) { unreachable ; }
    var word: u64 = 2 ;
    const landing_pad_type: u32 = if (indirection) 1 else 0 ;
    word |= base.Masks.farptr_pad    & (@intCast(usize, landing_pad_type) << 2) ;
    word |= base.Masks.farptr_offset & (@intCast(usize, landing_pad_off)  << 3) ;
    word |= base.Masks.farptr_id     & (@intCast(usize, landing_pad_seg) << 32) ;
    std.mem.writeIntLittle(u64, bytes, word) ;
}


///
/// Right now this is used only for decoding ; cleary this could be
/// more elegantly put to use. Anw. The size of each element is the
/// sum of data_size and ptrs_size.
///
const StructPointer = struct {
    data_size: u32,
    ptrs_size: u32,
    offset_in_words: u32,
} ;

const ListPointer = struct {
    len: u32,
    data_size: u32,
    ptrs_size: u32,
    offset_in_words: u32,
    contains_composites: bool,
} ;

pub fn decodeStructPointer(bytes: []const u8) StructPointer {
    if (bytes.len < bytes_per_word) {
        unreachable ;
    }
    // The alignment cast *should* be safe because the capnp layout
    // revolves around words, but it's the only guaranty.
    const word: u64 = @ptrCast(*const u64, @alignCast(@alignOf(*const u64), bytes)).* ;
    var ptr: StructPointer = undefined ;
    ptr.offset_in_words = @intCast(u32, (word & base.Masks.struct_offset) >> 2) ;
    ptr.data_size       = @intCast(u32, (word & base.Masks.struct_datasec) >> 32) ;
    ptr.ptrs_size       = @intCast(u32, (word & base.Masks.struct_ptrsec) >> 48) ;
    return ptr ;
}

pub fn decodeListPointer(bytes: []const u8) ListPointer {
    if (bytes.len < bytes_per_word * 2) {
        unreachable ; // Trust in the programmer, you know.
    }
    // The alignment cast *should* be safe because the capnp layout
    // revolves around words, but it's the only guaranty.
    const word: u64 = @ptrCast(*const u64, @alignCast(@alignOf(*const u64), bytes)).* ;
    var ptr: ListPointer = undefined ;
    ptr.offset_in_words = @intCast(u32, (word & base.Masks.list_offset) >> 2) ;
    ptr.data_size       = @intCast(u32, (word & base.Masks.list_elem_size) >> 32) ;
    ptr.len             = @intCast(u32, (word & base.Masks.list_size) >> 35) ;
    // If the list is a list of composites, parse the tag word.
    if (ptr.data_size == 7) {
        const tag = decodeStructPointer(bytes[bytes_per_word..bytes_per_word*2]) ;
        ptr.len = tag.offset_in_words ;
        ptr.data_size = tag.data_size ;
        ptr.ptrs_size = tag.ptrs_size ;
        ptr.contains_composites = true ;
    } else {
        ptr.data_size = decodeNormalListElementSize(ptr.data_size);
        ptr.ptrs_size = 0 ;
        ptr.contains_composites = false ;
    }
    return ptr ;
}


pub const Heuristic = union(enum) {
    nostorage: void,
    percentage: u32, // Litteraly in %, discuss before making it a float.
    threshold: u32,
} ;

///
/// A segmented buffer. This is an extensible buffer that can grow segment
/// after segment, keeping the previous segments unchanged to avoid pointer
/// invalidation.
///
/// It is intended to be used as an allocator. It doesn't provide the true
/// Zig interface to Allocators, because it would be pointless, but an alloc
/// function is provided, that scatters data on segments with an heuristic.
///
/// The heuristic is the following : we have regular segments and "storage"
/// segments, dedicated to store objects with a size superior to a certain
/// proportion of the max segment size.
///
/// Do note that this implies it is possible for some data to exist in a
/// segment *preceding* the segment in which the pointer resides. I didn't
/// try to prevent that as is it not specifically forbidden.
///
pub const MessageBuffer = struct {
    max_segment_size: u32 = 1024,
    underlying_allocator: Allocator,
    segments: ArrayList(ArrayList(u8)),
    allocators: ArrayList(FixedAllocator),
    regular_ids: ArrayList(u32),

    /// Controls the behaviour of the allocator. This can totally be changed
    /// during usage. Results will be strange but it is permitted.
    heuristic: Heuristic,

    const Self = @This() ;
    const FixedAllocator = std.heap.FixedBufferAllocator ;

    ///
    /// Initialize the segments with a pre-allocated buffer, and no segment.
    ///
    pub fn init(max_segment_size: u32, allocator: Allocator) !Self {
        var buffer : Self = undefined ;
        buffer.underlying_allocator = allocator ;
        buffer.max_segment_size = max_segment_size ;
        buffer.segments = ArrayList(ArrayList(u8)).init(allocator) ;
        buffer.allocators = ArrayList(FixedAllocator).init(allocator) ;
        buffer.regular_ids = ArrayList(u32).init(allocator) ;
        buffer.heuristic = .{ .percentage = 10 } ;
        _ = try buffer.addSegment() ;
        _ = try buffer.alloc(bytes_per_word, 0, 0) ; // Allocate the first pointer.
        return buffer ;
    }

    ///
    /// Destroys everything.
    ///
    pub fn deinit(self: *Self) void {
        self.regular_ids.deinit() ;
        self.allocators.deinit() ;
        self.segments.deinit() ;
    }

    ///
    /// Returns the slice of bytes for a given segment and offset.
    ///
    pub fn getSlice(self: *Self, segment: u32, pos: u32, size: u32) ![]u8 {
        return self.segments.items[segment].items[pos..pos+size] ;
    }

    ///
    /// Append a new segment and corresponding fixed allocator. No
    /// verification is performed regarding the possible emptyness of
    /// already existing segments.
    ///
    pub fn addSegment(self: *Self) !u32 {
        const n = @intCast(u32, self.segments.items.len) ;
        try self.segments.resize(n + 1) ;
        try self.allocators.resize(n + 1) ;
        // Create the new segments buffer.
        self.segments.items[n] = ArrayList(u8).init(self.underlying_allocator) ;
        try self.segments.items[n].resize(self.max_segment_size) ;
        // Instanciate the fixed allocator.
        var segment = self.segments.items[n].items[0..] ;
        self.allocators.items[n] = FixedAllocator.init(segment) ;
        // Add to the regular segments IDs list.
        try self.regular_ids.append(n) ;
        return n ;
    }

    ///
    /// Adds a new segments and records it in the storage IDs list.
    ///
    pub fn addStorageSegment(self: *Self) !u32 {
        const s = self.addSegment() ;
        _ = self.regular_ids.pop() ;
        return s ;
    }

    ///
    /// Shrink segments to match the length of the fixed allocators, since
    /// their current sizes are the maximum segment size, to allow the fixed
    /// allocators to work properly.
    ///
    fn shrinkSegments(self: *Self) void {
        for (self.segments.items) |*segment, i| {
            const len = self.allocators.items[i].end_index ;
            segment.*.shrinkRetainingCapacity(len) ;
        }
    }

    ///
    /// Write segments using the given writer.
    /// TODO Test (I know it works because it's historical copy, but still...)
    ///
    pub fn feedTo(self: *Self, writer: anytype) !void {
        self.shrinkSegments() ;
        // Write segments infos.
        try writer.writeIntLittle(u32, @intCast(u32, self.segments.items.len - 1)) ;
        //std.debug.print("Write {} segments.\n", .{self.segments.items.len}) ;
        for (self.segments.items) |segment| {
            try writer.writeIntLittle(u32, @intCast(u32, segment.items.len / bytes_per_word)) ;
            //std.debug.print("Write segment's length {}.\n", .{segment.items.len / bytes_per_word}) ;
        }
        // Add padding if required.
        if ((self.segments.items.len & 1) == 0) {
            try writer.writeIntLittle(u32, 0) ;
            //std.debug.print("Write padding half word.\n", .{}) ;
        }
        // Write segments.
        for (self.segments.items) |segment| {
            //std.debug.print("Write segment of length {}.\n", .{segment.items.len / bytes_per_word}) ;
            try writer.writeAll(segment.items) ;
        }
    }

    ///
    /// A Pointer stores the information that is common to all pointers in a
    /// message, that is, the address of the pointer, the adress of the content,
    /// and the segment in which the addresses are specified.
    ///
    /// This is poorly named, but I'm not willing to spend more time on this
    /// currently.
    ///
    pub const Pointer = struct {
        ptr_segment: u32,
        data_segment: u32,
        ptr_pos: u32,
        data_pos: u32,
    } ;

    fn candidateForStorage(self: Self, size: u32) bool {
        return switch (self.heuristic) {
            Heuristic.nostorage => false,
            Heuristic.percentage => |p| (size*100/self.max_segment_size) >= p,
            Heuristic.threshold => |t| size >= t,
        } ;
    }

    fn findAdequateStorageSegment(self: *Self, size: u32) !u32 {
        const n = self.segments.items.len ;
        var i: usize = 0 ;
        var j: usize = 0 ;
        // This is valid because the ids are sorted. For the intuition i is
        // simply iterating over the implicitely defined storage ids.
        while (i < n) : (i += 1) {
            while (i <= self.regular_ids.items[j]) : (i += 1) {}
            j += 1 ;
            if (i < n) {
                const remaining = self.max_segment_size -
                    self.allocators.items[i].end_index ;
                if (size <= remaining) {
                    return @intCast(u32, i) ;
                }
            }
        }
        return self.addStorageSegment() ;
    }

    fn findAdequateRegularSegment(self: *Self, size: u32) !u32 {
        for (self.regular_ids.items) |i| {
            const remaining = self.max_segment_size -
                self.allocators.items[i].end_index ;
            if (size <= remaining) {
                return @intCast(u32, i) ;
            }
        }
        return self.addSegment() ;
    }

    ///
    /// Reserves new data on a segment and returns the pointer.
    ///
    pub fn alloc(self: *Self, size: u32, seg: u32, ptr_pos: u32) !Pointer {
        // Check that the alloc request is sound.
        if (size > self.max_segment_size) {
            return Error.TooLargeForSegment ;
        }
        var updated_ptr_segment = seg ;
        var updated_ptr_pos = ptr_pos ;
        var target_segment: u32 = undefined ;
        const for_storage = self.candidateForStorage(size) ;
        const remaining = self.max_segment_size - self.allocators.items[seg].end_index ;
        // If we must allocate on another segment, we must create an inter-
        // segment pointer, and then allocate a new word-sized pointer before
        // allocating the requested size.
        if ((for_storage) or (size > remaining)) {
            // Find the segment...
            target_segment = try
                if (for_storage) self.findAdequateStorageSegment(size)
                else self.findAdequateRegularSegment(size) ;
            // Allocate a landing pointer in it.
            var allocator = self.allocators.items[target_segment].allocator() ;
            const segment_bytes = self.segments.items[target_segment].items[0..1] ;
            const ptr_bytes = try allocator.alloc(u8, bytes_per_word) ;
            updated_ptr_segment = target_segment ;
            updated_ptr_pos = offsetBetween(segment_bytes, ptr_bytes) ;
            // And prepare the inter-segment pointer.
            var far_ptr_bytes = self.segments.items[seg]
                .items[ptr_pos..ptr_pos + bytes_per_word][0..bytes_per_word] ;
            encodeFarPointer(updated_ptr_segment, updated_ptr_pos, far_ptr_bytes, false) ;
        } else {
            target_segment = seg ;
        }
        // Now, in all cases, we can allocate the requested size in target.
        var allocator = self.allocators.items[target_segment].allocator() ;
        const segment_bytes = self.segments.items[target_segment].items[0..1] ;
        const bytes = try allocator.alloc(u8, size) ;
        const data_pos = offsetBetween(segment_bytes, bytes) ;
        // And construct the pointer for it.
        return Pointer
            { .ptr_segment = updated_ptr_segment
            , .data_segment = target_segment
            , .ptr_pos = updated_ptr_pos
            , .data_pos = data_pos
            } ;
    }
} ;

test "message buffer extends correctly" {
    const bw: usize = bytes_per_word ;
    var buffer = try MessageBuffer.init(100*bw, std.heap.c_allocator) ;
    defer buffer.deinit() ;
    buffer.heuristic = .{ .percentage = 10 } ;
    // Allocate the first word.
    var ptr = try buffer.alloc(1*bw, 0, 0) ;
    // Allocate bytes on the first segment.
    ptr = try buffer.alloc(3*bytes_per_word, 0, 0) ;
    ptr = try buffer.alloc(4*bytes_per_word, 0, 0) ;
    try std.testing.expect(buffer.segments.items.len == 1) ;
    //std.debug.print("-> {}\n", .{buffer.allocators.items[0].end_index}) ;
    try std.testing.expect(buffer.allocators.items[0].end_index == 9*bw) ; // FIXME, WHY +1
    // Allocate bytes that should go on storage.
    ptr = try buffer.alloc(10*bw, 0, 0) ;
    ptr = try buffer.alloc(50*bw, 0, 0) ;
    try std.testing.expect(buffer.segments.items.len == 2) ;
    try std.testing.expect(buffer.regular_ids.items.len == 1) ;
    try std.testing.expect(buffer.regular_ids.items[0] == 0) ;
    // Allocate 72 words on the first segment.
    var i: u32 = 0 ; while (i < 8) : (i += 1) {
        ptr = try buffer.alloc(9*bytes_per_word, 0, 0) ;
    }
    try std.testing.expect(buffer.segments.items.len == 2) ;
    try std.testing.expect(buffer.allocators.items[0].end_index == 81*bw) ; // FIXME, WHY +1
    // Trigger another storage segment.
    ptr = try buffer.alloc(39*bw, 0, 0) ;
    try std.testing.expect(buffer.segments.items.len == 3) ;
    try std.testing.expect(buffer.regular_ids.items.len == 1) ;
    try std.testing.expect(buffer.allocators.items[1].end_index == 62*bw) ;
    try std.testing.expect(buffer.allocators.items[2].end_index == 40*bw) ;
    // Trigger another regular segment by changing the heuristic.
    buffer.heuristic = .{ .nostorage = {} } ;
    ptr = try buffer.alloc(21*bw, 0, 0) ;
    buffer.heuristic = .{ .percentage = 10 } ;
    try std.testing.expect(buffer.segments.items.len == 4) ;
    try std.testing.expect(buffer.regular_ids.items.len == 2) ;
    try std.testing.expect(buffer.regular_ids.items[1] == 3) ;
    try std.testing.expect(buffer.allocators.items[3].end_index == 22*bw) ;
    // Assess that the first segment is still filled for a small enough alloc.
    ptr = try buffer.alloc(7*bw, 0, 0) ;
    ptr = try buffer.alloc(7*bw, 0, 0) ;
    try std.testing.expect(buffer.segments.items.len == 4) ;
    try std.testing.expect(buffer.regular_ids.items.len == 2) ;
    try std.testing.expect(buffer.allocators.items[0].end_index == 95*bw) ; // FIXME, WHY +1
    try std.testing.expect(buffer.allocators.items[3].end_index == 22*bw) ;
}

test "message buffer pointer correctness" {
    const bw: usize = bytes_per_word ;
    var buffer = try MessageBuffer.init(100*bw, std.heap.c_allocator) ;
    defer buffer.deinit() ;
    buffer.heuristic = .{ .percentage = 10 } ;
    // Allocate the first word.
    var ptr = try buffer.alloc(1*bw, 0, 0) ;
    // Allocate various regions in the current segment and check pointers.
    ptr = try buffer.alloc(4*bw, 0, 0*bw) ; try std.testing.expect(ptr.ptr_segment == 0) ; try std.testing.expect(ptr.ptr_pos == 0*bw) ; try std.testing.expect(ptr.data_segment == 0) ; try std.testing.expect(ptr.data_pos == 2*bw) ;
    ptr = try buffer.alloc(1*bw, 0, 1*bw) ; try std.testing.expect(ptr.ptr_segment == 0) ; try std.testing.expect(ptr.ptr_pos == 1*bw) ; try std.testing.expect(ptr.data_segment == 0) ; try std.testing.expect(ptr.data_pos == 6*bw) ;
    ptr = try buffer.alloc(1*bw, 0, 2*bw) ; try std.testing.expect(ptr.ptr_segment == 0) ; try std.testing.expect(ptr.ptr_pos == 2*bw) ; try std.testing.expect(ptr.data_segment == 0) ; try std.testing.expect(ptr.data_pos == 7*bw) ;
    ptr = try buffer.alloc(1*bw, 0, 3*bw) ; try std.testing.expect(ptr.ptr_segment == 0) ; try std.testing.expect(ptr.ptr_pos == 3*bw) ; try std.testing.expect(ptr.data_segment == 0) ; try std.testing.expect(ptr.data_pos == 8*bw) ;
    ptr = try buffer.alloc(1*bw, 0, 4*bw) ; try std.testing.expect(ptr.ptr_segment == 0) ; try std.testing.expect(ptr.ptr_pos == 4*bw) ; try std.testing.expect(ptr.data_segment == 0) ; try std.testing.expect(ptr.data_pos == 9*bw) ;
    // ............................................................................................................................................................................................... THOSE ARE +1, WHY, FIXME ^ (BUT NOT THOSE BELOW...)
    // Allocate various regions in other segments and check pointers.
    ptr = try buffer.alloc(12*bw, 0, 5*bw) ; try std.testing.expect(ptr.ptr_segment == 1) ; try std.testing.expect(ptr.ptr_pos == 0*bw)  ; try std.testing.expect(ptr.data_segment == 1) ; try std.testing.expect(ptr.data_pos == 1*bw) ;
    ptr = try buffer.alloc(24*bw, 0, 6*bw) ; try std.testing.expect(ptr.ptr_segment == 1) ; try std.testing.expect(ptr.ptr_pos == 13*bw) ; try std.testing.expect(ptr.data_segment == 1) ; try std.testing.expect(ptr.data_pos == 14*bw) ;
}


///
/// A fake type that is used to recognise a List(Struct) from other
/// primitives Lists, since List(Struct) are encoded with composite
/// types, and *not* as list of pointers.
///
pub const Struct = struct {} ;


///
/// The stencil for a list object. It only stores the location of the
/// object, and not the pointer.
///
/// List is the only parameterised type *that should be able to take
/// any type as parameter*. Generics are handled differently. To tell
/// the truth they are not handled at all currently, but anyway, the
/// documentation specifies that they only accept pointer types.
///
/// This previous point raises the question of how to handle the
/// List(List(T)) and other fancy List(List(List(...(List(T))))) cases
/// that will arise. With practical considerations, I've settled for
/// an implementation that will seem strange if not motivated. There
/// are three distinct types for lists, because it simplifies types
/// quite a lot (à priori, of course) :
///
/// - List(T) where T is a primitive type
/// - ListOfList(T) = List(List(T))
/// - NestedLists(T) = List(List(List(T))) and more
///
/// Essentially the three types propose distinct operations, which
/// are as follows :
///
/// - List(T).get :: Int -> T
/// - ListOfList(T).get :: Int -> List(T)
/// - NestedLists(T).get :: Int -> ListOfList(T)
/// - NestedLists(T).getSub :: Int -> NestedLists(T)
///
/// Now that WTF is written plain big on your retinas, allow me to
/// explain. We could certainly implement a single type, that should
/// know at runtime what to do depending on the level of nesting.
/// Using a template type List(T).Stencil would still require some
/// runtime cost to determine what must be returned on get(). Instead,
/// we can have two distinct types : List(T) and NestedLists(T), where
/// NestedLists(T) alone has to have some runtime indication of what to
/// return. I introduce an intermediary ListOfList(T) for a specific
/// reason : ListOfList(T) will be fairly common, so we can simplify
/// the user API a bit, in the sense that the chance for an user to be
/// required to use the weird NestedLists trick is decreased.
///
/// Note that the List(Text) and List(Data) are handled as List(T).
///
/// I know that we *could* find a metaprogramming way of doing pretty
/// much everything at comptime. However this, imho, would be much
/// less simple. That being said, this feels uncomfortable so any
/// discussion on the topic will be an improvement :) I suppose that
/// looking at the rust version would also be informative, but I find
/// the code unwieldy. Also sorry for this massive comment block.
///
pub fn List(comptime T: type) type {
    return struct {
        pub const Stencil = struct {
            message: *MessageBuffer,
            segment: u32,
            pos: u32,
            len: u32,
            data_size: u32, // In *bytes*.
            ptrs_size: u32, // In *bytes*, 0 if list of primitives.

            ///
            /// Initialize a list of length len *on the buffer*. This
            /// is quite different from just initializing the Stencil
            /// structure (which can be done with initFromPointer).
            /// The names are not terrible, I know.
            ///
            pub fn initFrom(
                buffer: *MessageBuffer,
                ptr_seg: u32,
                ptr_pos: u32,
                elem_size: u32, // In bytes, ignored if T is bool.
                len: u32,
            ) !Stencil {
                return initCompositeFrom(buffer, ptr_seg, ptr_pos, elem_size, 0, len) ;
            }

            ///
            /// Same as its sibling initFrom, but handles the more
            /// general case of composite elements (thus having two
            /// distinct data and ptrs sizes).
            ///
            pub fn initCompositeFrom(
                buffer: *MessageBuffer,
                ptr_seg: u32,
                ptr_pos: u32,
                data_section_size: u32, // In *bytes*, ignored if T is bool.
                ptr_section_size: u32, // In *bytes*, ignored if T isn't Struct.
                len: u32,
            ) !Stencil {
                //std.debug.print("\nInitializing list of length {}.\n", .{len}) ;
                const is_primitive = comptime isPrimitive(T) ;
                //std.debug.print("List of primitives: {}.\n", .{is_primitive}) ;
                const elem_size = data_section_size + ptr_section_size ;
                //const elem_size = 8 ; // FIXME
                //std.debug.print("Element size: {}.\n", .{elem_size}) ;
                var elem_type: ListElementType = undefined ;
                var bytes_size: u32 = undefined ;
                if (T == bool) {
                    elem_type = ListElementType.Bool ;
                    bytes_size = len / 8 ;
                } else if (is_primitive) {
                    elem_type = ListElementType.Normal ;
                    bytes_size = len * elem_size ;
                } else {
                    elem_type = ListElementType.Composite ;
                    bytes_size = len * elem_size + bytes_per_word ;
                }
                // Compute the padded size.
                const overflow = @rem(bytes_size, bytes_per_word) ;
                const bytes_size_padded = bytes_size + if (overflow > 0) bytes_per_word - overflow else 0 ;
                //std.debug.print("Bytes size:          {}.\n", .{bytes_size}) ;
                //std.debug.print("Bytes size (padded): {}.\n", .{bytes_size_padded}) ;
                // Allocate memory.
                const ptr = try buffer.alloc(bytes_size_padded, ptr_seg, ptr_pos) ;
                const s = ptr.data_segment ;
                const p = ptr.data_pos ;
                // Zero-initialise memory.
                var bytes = buffer.segments.items[s].items[p..p + bytes_size_padded] ;
                for (bytes) |*byte| { byte.* = 0 ; }
                // Encode the list pointer.
                const ps = ptr.ptr_segment ;
                const pp = ptr.ptr_pos ;
                var ptr_bytes = buffer.segments.items[ps].items[pp..pp + bytes_per_word] ;
                const offset = offsetBetween(ptr_bytes, bytes) - bytes_per_word ;
                encodeListPointer(offset, len, elem_size, elem_type, ptr_bytes[0..bytes_per_word]) ;
                // Encode the composite tag if required.
                if (elem_type == ListElementType.Composite) {
                    const data_words = data_section_size / bytes_per_word ;
                    const ptrs_words = ptr_section_size  / bytes_per_word ;
                    encodeStructPointer(len, data_words, ptrs_words, bytes[0..bytes_per_word]) ;
                }
                // Return the stencil.
                return Stencil
                    { .message = buffer
                    , .segment = s
                    , .pos = p
                    , .len = len
                    , .data_size = data_section_size
                    , .ptrs_size = ptr_section_size
                    } ;
            }

            ///
            /// Slightly different from the other two: this one does
            /// *not* do anything to the buffer ; instead it just
            /// reads an *existing* pointer and build the stencil
            /// accordingly (the pointer directly corrsponds to
            /// memory while the stencil is what the user handles).
            ///
            pub fn initFromPointer(
                buffer: *MessageBuffer,
                ptr_seg: u32,
                ptr_pos: u32,
            ) !Stencil {
                // FIXME The 2*bytes_per_word may fail in very specific
                // cases, and also it's not very clean.
                const bytes = try buffer.getSlice(ptr_seg, ptr_pos, 2*bytes_per_word) ;
                const ptr = decodeListPointer(bytes) ;
                return Stencil
                    { .message = buffer
                    , .segment = ptr_seg
                    , .pos = ptr_pos + ptr.offset_in_words * bytes_per_word
                    , .len = ptr.len
                    , .data_size = ptr.data_size
                    , .ptrs_size = ptr.ptrs_size
                    } ;
            }

            ///
            /// Retrieve the i-th element in the list.
            ///
            pub fn get(self: Stencil, i: u32) !PrimitiveOrStencil(T) {
                if (i >= self.len) {
                    return Error.OutOfBounds ;
                }
                // Because of the List/ListOfList/NestedList distinction,
                // if the element type is not a primitive, it is a struct.
                const is_primitive = comptime isPrimitive(T) ;
                // Compute the offset and return.
                const s = self.segment ;
                if (is_primitive) {
                    const begin = self.pos + @sizeOf(PrimitiveOrStencil(T)) * i ;
                    const end   = self.pos + @sizeOf(PrimitiveOrStencil(T)) * (i + 1) ;
                    const bytes = self.message.segments.items[s].items[begin..end] ;
                    return getInternal(PrimitiveOrStencil(T), bytes, "", 0) ;
                } else {
                    var stencil: T.Stencil = undefined ;
                    stencil.message = self.message ;
                    stencil.segment = self.segment ;
                    stencil.data = bytes_per_word + self.pos + i * (self.data_size + self.ptrs_size) ;
                    stencil.ptrs = stencil.data + self.data_size ;
                    return stencil ;
                }
            }

            ///
            /// Set the i-th element in the list.
            ///
            pub fn set(self: Stencil, i: u32, v: PrimitiveOrStencil(T)) !void {
                if (i >= self.len) {
                    return Error.OutOfBounds ;
                }
                // Because of the List/ListOfList/NestedList distinction,
                // if the element type is not a primitive, it is a struct.
                const is_primitive = comptime isPrimitive(T) ;
                // Compute the offset and return.
                if (is_primitive) {
                    const s = self.segment ;
                    const begin = self.pos + @sizeOf(T) * i ;
                    const end   = self.pos + @sizeOf(T) * (i + 1) ;
                    const bytes = self.message.segments.items[s].items[begin..end] ;
                    return setInternal(T, v, bytes, "", 0) ;
                } else {
                    @compileError("Set element in list of structs not supported.") ;
                }
            }

            pub fn getSlice(self: Stencil, i: u32, dest: []T) !void {
                if (i + dest.len > self.len) {
                    return Error.OutOfBounds ;
                }
                const s = self.segment ;
                const is_primitive = comptime isPrimitive(T) ;
                if (! is_primitive) {
                    @compileError("getSlice not (yet) supported on structs/lists/pointers.") ;
                }
                // TODO Check if a simple memcpy would comply with
                // endianness.
                for (dest) |*e, j| {
                    const begin = self.pos + @sizeOf(T) * (i + j) ;
                    const end   = self.pos + @sizeOf(T) * (i + j + 1) ;
                    const bytes = self.message.segments.items[s].items[begin..end] ;
                    e.* = try getInternal(T, bytes, "", 0) ;
                }
            }

            pub fn setSlice(self: Stencil, i: u32, src: []const T) !void {
                if (i + src.len > self.len) {
                    return Error.OutOfBounds ;
                }
                const s = self.segment ;
                const is_primitive = comptime isPrimitive(T) ;
                if (! is_primitive) {
                    @compileError("setSlice not (yet) supported on structs/lists/pointers.") ;
                }
                // TODO Check if a simple memcpy would comply with
                // endianness.
                for (src) |e, j| {
                    const begin = self.pos + @sizeOf(T) * (i + j) ;
                    const end   = self.pos + @sizeOf(T) * (i + j + 1) ;
                    const bytes = self.message.segments.items[s].items[begin..end] ;
                    try setInternal(T, e, bytes, "", 0) ;
                }
            }
        } ;
    } ;
}

pub fn ListOfLists(comptime T: type) type {
    return struct {
        pub const Stencil = struct {
            message: *MessageBuffer,
            segment: u32,
            pos: u32,
            len: u32,

            pub fn initFrom(
                buffer: *MessageBuffer,
                ptr_seg: u32,
                ptr_pos: u32,
                elem_size: u32, // In bytes, ignored if T is bool.
                len: u32,
            ) !Stencil {
                _ = T ;
                _ = len ;
                _ = buffer ;
                _ = ptr_pos ;
                _ = ptr_seg ;
                _ = elem_size ;
                //return initCompositeFrom(buffer, ptr_seg, ptr_pos, elem_size, 0, len) ;
                return Error.BadGetMethodForNestedList ;
            }

            // pub fn get(self: Stencil, i: u32) !List(T).Stencil {
            //     // ...
            // }

            // pub fn set(self: Stencil, i: u32, l: List(T).Stencil) !void {
            //     if (i + l.len > self.len) {
            //         return Error.OutOfBounds ;
            //     }
            //     // ...
            // }
        } ;
    } ;
}

///
/// Blobs are not aliases of List(u8) because I anticipate that at some
/// point we should have some validation of the content, and handling
/// special cases for the null terminator.
///
pub const Blob = struct {
    pub const Stencil = struct {
        list: List(u8).Stencil,
    
        pub fn initFrom(
            buffer: *MessageBuffer,
            ptr_seg: u32,
            ptr_pos: u32,
            len: u32,
        ) !Stencil {
            const ls = try List(u8).Stencil.initFrom(buffer, ptr_seg, ptr_pos, 1, len) ;
            return Stencil { .list = ls } ;
        }

        pub fn initFromPointer(
            buffer: *MessageBuffer,
            ptr_seg: u32,
            ptr_pos: u32,
        ) !Stencil {
            const ls = try List(u8).Stencil.initFromPointer(buffer, ptr_seg, ptr_pos) ;
            return Stencil { .list = ls } ;
        }

        pub fn get(self: Stencil, i: u32) !u8 {
            return self.list.get(i) ;
        }

        pub fn set(self: Stencil, i: u32, e: u8) !void {
            return self.list.set(i, e) ;
        }

        pub fn getSlice(self: Stencil, i: u32, e: []u8) !void {
            return self.list.getSlice(i, e) ;
        }

        pub fn setSlice(self: Stencil, i: u32, e: []const u8) !void {
            return self.list.setSlice(i, e) ;
        }
    } ;
} ;

pub const Data = Blob ;
pub const Text = Blob ;

///
/// This stencil is specific to List(List(List(...(T)))). Even though
/// you could build a regular List(T).Stencil for it, you wouldn't
/// dispose of the relevant operators.
///
/// In terms of state it is identical to List with the addition of a
/// level. List(T) is 0, List(List(T)) is 1, List(List(List(T))) is 2
/// and so forth. This is used to determine what get method you should
/// use. If level == 1, use get() and obtain a List(T).Stencil. If
/// level > 1, use getSub() and obtain another NestedLists with a
/// decremented level.
///
pub fn NestedLists(comptime T: type) type {
    return struct {
        pub const Stencil = struct {
            message: *MessageBuffer,
            segment: u32,
            pos: u32,
            len: u32,
            level: u32,
        } ;

        ///
        /// Initialize a list of len lists.
        ///
        pub fn initFrom(
            buffer: *MessageBuffer,
            ptr_seg: u32,
            ptr_pos: u32,
            len: u32,
            level: u32,
        ) !Stencil {
            _ = buffer ;
            _ = ptr_seg ;
            _ = ptr_pos ;
            _ = len ;
            _ = level ;
            return Error.ListTooLong ; // FIXME
        }

        ///
        /// Initialise a sublist if level = 1 (that is, if the sublist
        /// is going to be a List(T). Otherwise use the initSub method.
        ///
        pub fn init(self: Stencil, i: u32, len: u32) !List(T).Stencil {
            _ = self ;
            _ = len ;
            _ = i ;
        }

        ///
        /// Initialise a sublist if level > 1 (that is, if the sublist
        /// is also a NestedLists). Otherwise use the init method.
        ///
        pub fn initSub(self: Stencil, i: u32, len: u32) !NestedLists(T).Stencil {
            _ = self ;
            _ = len ;
            _ = i ;
        }

        ///
        /// Return a stencil over a sublist, when this sublist is just
        /// a List. Otherwise use the getSub method.
        ///
        pub fn get(self: Stencil, i: u32) !List.Stencil(T) {
            if (self.level != 1) {
                return Error.BadGetMethodForNestedList ;
            }
            if (i >= self.len) {
                return Error.OutOfBounds ;
            }
            return Error.BadGetMethodForNestedList ; // FIXME
        }

        ///
        /// Return a stencil over a sublist, when this sublist is also
        /// a NestedLists. Otherwise use the get method.
        ///
        pub fn getSub(self: Stencil, i: u32) !NestedLists.Stencil(T) {
            if (self.level < 2) {
                return Error.BadGetMethodForNestedList ;
            }
            if (i >= self.len) {
                return Error.OutOfBounds ;
            }
            return Error.BadGetMethodForNestedList ; // FIXME
        }

        //pub fn set(self: *Stencil, i: u32, WHAT ?) !void {
        //    // ...
        //}
    } ;
}

fn isPrimitive(comptime T: type) bool {
   return switch (T) {
       bool, i8, u8, i16, u16, i32, u32, i64, u64 => true,
       else => false
   } ;
}

///
/// Returns the stencils type associated with a given type.
///
fn PrimitiveOrStencil(comptime T: type) type {
    return if (isPrimitive(T)) T else T.Stencil ;
}

fn xorBytesSlices(a: []u8, b: []const u8) void {
    if (b.len == 0)
        return ;
    if (a.len != b.len)
        unreachable ;
    for (a) |*c, i| {
        c.* ^= b[i] ;
    }
}

///
/// Set the memory region corresponding to a field given a reference
/// value. The offset is *provided in bits* when the type is bool.
/// 
/// If the default value slice is non-empty (and assumed of the same
/// length as bytes) it is xored with the passed value, as required
/// by how capnproto handles default values.
///
pub fn setInternal(
    comptime T: type,
    value: T,
    bytes: []u8,
    default: []const u8,
    offset: u64
) !void {
    if (T == bool) {
        // TODO Default value
        // Offset is in bits.
        const offset_bytes = @divTrunc(offset, 8) ;
        const rem_bits = @intCast(u3, @rem(offset, 8)) ;
        const one: u8 = 1 ;
        if (value) {
            bytes[offset_bytes] |= (one << rem_bits) ;
        } else {
            bytes[offset_bytes] &= (~(one << rem_bits)) ;
        }
        return ;
    }
    var dest = bytes[offset..] ;
    switch (T) {
        f32 => {
            const int = @bitCast(u32, value) ;
            std.mem.writeIntLittle(u32, dest[0..@sizeOf(T)], int) ;
            xorBytesSlices(dest[0..@sizeOf(T)], default) ;
        },
        f64 => {
            const int = @bitCast(u64, value) ;
            std.mem.writeIntLittle(u64, dest[0..@sizeOf(T)], int) ;
            xorBytesSlices(dest[0..@sizeOf(T)], default) ;
        },
        i8, i16, i32, i64, u8, u16, u32, u64 => {
            std.mem.writeIntLittle(T, dest[0..@sizeOf(T)], value) ;
            xorBytesSlices(dest[0..@sizeOf(T)], default) ;
        },
        else => unreachable,
    }
}

///
/// This function will check bounds and read the various types from the
/// buffer. All the generated getters will in fact call this function.
/// The offset is *provided in bits* when the type is bool.
///
/// If the default value slice is non-empty (and assumed of the same
/// length as bytes) it is xored with the returned value, as required
/// by how capnproto handles default values.
///
pub fn getInternal(
    comptime T: type,
    bytes: []const u8,
    default: []const u8,
    offset: u64
) !T {
    var buffer: [8]u8 = undefined ;
    // TODO Check bounds ^^
    if (T == bool) {
        // TODO Default value
        // Offset is in bits.
        const offset_bytes = @divTrunc(offset, 8) ;
        const rem_bits = @intCast(u3, @rem(offset, 8)) ;
        const one: u8 = 1 ;
        return (bytes[offset_bytes] & (one << rem_bits)) != 0 ;
    }
    var source = bytes[offset..] ;
    const size = comptime @sizeOf(T) ;
    switch (T) {
        f32 => {
            std.mem.copy(u8, buffer[0..size], source[0..size]) ;
            xorBytesSlices(buffer[0..size], default) ;
            const int = std.mem.readIntLittle(u32, buffer[0..size]) ;
            return @bitCast(T, int) ;
        },
        f64 => {
            std.mem.copy(u8, buffer[0..size], source[0..size]) ;
            xorBytesSlices(buffer[0..size], default) ;
            const int = std.mem.readIntLittle(u64, buffer[0..size]) ;
            return @bitCast(T, int) ;
        },
        i8, i16, i32, i64, u8, u16, u32, u64 => {
            std.mem.copy(u8, buffer[0..size], source[0..size]) ;
            xorBytesSlices(buffer[0..size], default) ;
            return std.mem.readIntLittle(T, buffer[0..size]) ;
        },
        else => unreachable,
    }
}

