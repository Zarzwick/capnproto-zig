const std = @import("std") ;

const bytes_per_word = @import("base.zig").bytes_per_word ;

///
/// ...
///
pub fn Decompressor(comptime UnderlyingReader: type) type {
    return struct {
        const Self = @This() ;
        const DecodingError = error { WrongNumberOfBytes } ;
        const Error = UnderlyingReader.Error || DecodingError ;
        const Reader = std.io.Reader(*Self, anyerror, decompress) ;

        underlying_reader : UnderlyingReader,
        total_bytes_read : usize,
        bytes_read : usize,

        const State = enum { per_word, zeroes_sequence, unpackable } ;

        //
        // This should returns something in [1, 8[.
        //
        fn decompressWord(
            self: *Self,
            tag: u8,
            bytes: []u8,
        ) !usize {
            comptime var i : u8 = 0 ;
            inline while (i < bytes_per_word) : (i += 1) {
                if ((tag & (0x1 << i)) != 0) {
                    try self.underlying_reader.readNoEof(bytes[i..i+1]) ;
                } else {
                    bytes[i] = 0 ;
                }
            }
            return 8 ;
        }

        fn decompressZeroRegion(self: *Self, bytes: []u8) !usize {
            var n : [1]u8 = undefined ;
            try self.underlying_reader.readNoEof(n[0..1]) ;
            n[0] += 1 ;
            var i : u8 = 0 ;
            while (i < n[0]) : (i += 1) {
                bytes[i] = 0 ;
            }
            return n[0] ;
        }

        fn decompressUnpackedRegion(self: *Self, bytes: []u8) !usize {
            // Copy the first word.
            try self.underlying_reader.readNoEof(bytes[0..bytes_per_word]) ;
            // Get n.
            var n : [1]u8 = undefined ;
            try self.underlying_reader.readNoEof(n[0..1]) ;
            // Copy those n extra words.
            const a = bytes_per_word ;
            const b = bytes_per_word * (n[0] + 1) ;
            try self.underlying_reader.readNoEof(bytes[a..b]) ;
            return bytes_per_word * (n[0] + 1) ;
        }

        ///
        /// ...
        ///
        fn decompress(self: *Self, bytes: []u8) !usize {
            //var buffer : [bytes_per_word]u8 = undefined ;
            var tag : [1]u8 = undefined ;
            var total_decompressd : usize = 0 ;
            while (true) {
                const bytes_read = try self.underlying_reader.read(tag[0..]) ;
                // This is a normal end of stream. It means that everything
                // was decompressed fine and is coherent with the state machine.
                if (bytes_read < 1) {
                    break ;
                }
                // Now, we try to get a number of decompressed bytes (the number
                // of bytes after expansion). The subfunctions may return an
                // error, as any eof or read miss would be inconsistent with
                // the reader state. The error is thus forwarded.
                const bytes_decompressd = switch (tag[0]) {
                    0x00 => try self.decompressZeroRegion(bytes[total_decompressd..]),
                    0xFF => try self.decompressUnpackedRegion(bytes[total_decompressd..]),
                    else => try self.decompressWord(tag[0], bytes[total_decompressd..]),
                } ;
                total_decompressd += bytes_decompressd ;
            }
            return total_decompressd ; // CHECK
        }

        pub fn reader(self: *Self) Reader {
            return .{ .context = self } ;
        }
    } ;
}
