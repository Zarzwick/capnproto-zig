pub const bytes_per_word: u8 = 8 ;

pub const Masks = struct {
    pub const object_type: u64      = 0x0000000000000003 ;

    pub const struct_offset: u64    = 0x00000000FFFFFFFC ;
    pub const struct_datasec: u64   = 0x0000FFFF00000000 ;
    pub const struct_ptrsec: u64    = 0xFFFF000000000000 ;

    pub const list_offset: u64      = 0x00000000FFFFFFFC ;
    pub const list_elem_size: u64   = 0x0000000700000000 ;
    pub const list_size: u64        = 0xFFFFFFF800000000 ;

    pub const farptr_pad: u64       = 0x0000000000000004 ;
    pub const farptr_offset: u64    = 0x00000000FFFFFFF0 ;
    pub const farptr_id: u64        = 0xFFFFFFFF00000000 ;
} ;

///
/// This function is meant to interpret elements size in lists.
///
pub fn interpretSize(coded_size: u64) u64 {
    const sizes = [_]u64 { 0, 1, 8, 16, 32, 64, 64 } ;
    return if (coded_size < 7) sizes[coded_size] else unreachable ; // FIXME
}
