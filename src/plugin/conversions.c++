///
/// ...
///
string lowercase(const string& s) {
	string copy = s ;
	for (size_t i = 0; i < copy.size(); ++ i) {
		copy[i] = std::tolower(copy[i]) ;
	}
	return copy ;
}

string uppercaseFirstLetter(const string& s) {
	string copy = s ;
	copy[0] = std::toupper(copy[0]) ;
	return copy ;
}
