#include <vector>
#include <string>
#include <locale>
#include <cassert>
#include <fstream>
#include <iostream>
#include <optional>
#include <unistd.h>
#include <unordered_map>

#include <capnp/schema.h>
#include <capnp/serialize.h>
#include <capnp/schema.capnp.h>
#include <capnp/schema-loader.h>

using namespace std ;
using namespace capnp ;
using namespace capnp::schema ;

using uint = unsigned int ;
using Request = CodeGeneratorRequest ;
using ReqFile = schema::CodeGeneratorRequest::RequestedFile ;

const char tab[] = "    " ;
const string ser_prefix   = "capnp.serialization." ;
const string deser_prefix = "capnp.deserialization." ;

#include "conversions.c++" // Bad, bad Hugo...

std::unordered_map<uint, uint> fields_indices ;
uint next_field_index ;

///
/// Possible error codes.
///
enum class ErrorCode
    { Ok = 0
    , Failed
    } ;

struct TypeSummary {
    bool is_primitive = false ;
    bool is_struct    = false ;
    bool is_pointer   = false ;
    bool is_list      = false ;
    bool is_text      = false ;
    bool is_data      = false ;
} ;

TypeSummary summarizeType(const capnp::Type& type) {
    TypeSummary summary ;
    switch (type.which()) {
        case capnp::schema::Type::LIST:
            summary.is_list    = true ;
            summary.is_pointer = true ;
            break ;
        case capnp::schema::Type::DATA:
            summary.is_data = true ;
            summary.is_pointer = true ;
            break ;
        case capnp::schema::Type::TEXT:
            summary.is_text = true ;
            summary.is_pointer = true ;
            break ;
        case capnp::schema::Type::STRUCT:
            summary.is_struct = true ;
            summary.is_pointer = true ;
            break ;
        case capnp::schema::Type::INTERFACE:
        case capnp::schema::Type::ANY_POINTER:
            summary.is_pointer = true ;
            break ;
        default:
            summary.is_primitive = true ;
            break ;
    }
    return summary ;
}


struct ListSummary {
    capnp::Type element_type ;
    uint depth = 0 ;
} ;

// Evaluate recursively the depth and base type of a list type. This
// is recDepthAndBaseForType in poc.hs. If this function already
// exists in the kj mess or in one of the APIs, I'm interested.
ListSummary summarizeList(const capnp::Type type, const uint d = 0) {
    if (type.which() == capnp::schema::Type::LIST) {
        return summarizeList(type.asList().getElementType(), d + 1) ;
    } else {
        return ListSummary { type, d } ;
    }
}


static_assert(schema::Type::VOID == 0, "Type enumerant doesn't start at 0.") ;
const char* type_names[19] =
    { "void"
    , "bool"
    , "i8"
    , "i16"
    , "i32"
    , "i64"
    , "u8"
    , "u16"
    , "u32"
    , "u64"
    , "f32"
    , "f64"
    // FIXME Just remove those...
    , "Text_UNREACHABLE"
    , "Data_UNREACHABLE"
    , "List_UNREACHABLE"
    , "Enum_UNREACHABLE"
    , "Struct_UNREACHABLE"
    , "Interface_UNREACHABLE"
    , "AnyPointer_UNREACHABLE"
    } ;

string showType(const capnp::Type& type, const string& prefix)
{
    if (type.which() == capnp::schema::Type::LIST) {
        ListSummary list_summary = summarizeList(type) ;
        const auto base = showType(list_summary.element_type, prefix) ;
        if (list_summary.depth == 1) {
            return prefix + "List(" + base + ")" ;
        } else if (list_summary.depth == 2) {
            return prefix + "ListOfLists(" + base + ")" ;
        } else {
            return prefix + "NestedLists(" + base + ")" ;
        }
    }
    switch (type.which()) {
        case capnp::schema::Type::STRUCT:
            return type.asStruct().getShortDisplayName().cStr() ;
        case capnp::schema::Type::TEXT:
            return prefix + "Text" ;
        case capnp::schema::Type::DATA:
            return prefix + "Data" ;
        case capnp::schema::Type::ENUM:
            return string(type.asEnum().getShortDisplayName().cStr()) ;
        default:
            return { type_names[type.which()] } ;
    }
}

// string nodeName(const capnp::Schema& schema) {
//     const auto display_name = schema.getProto().getDisplayName() ;
//     const auto prefix_len = schema.getProto().getDisplayNamePrefixLength() ;
//     const auto len = display_name.size() ;
//     const char* c_str = display_name.cStr() ;
//     return string(c_str + prefix_len, len - prefix_len) ;
// }

// void printTypeInfos(const capnp::Type& type)
// {
//     cerr << "Type : " ;
//     switch (type.which()) {
//         case capnp::schema::Type::ENUM:
//         case capnp::schema::Type::STRUCT:
//             cerr << "struct" << endl ;
//             break ;
//         case capnp::schema::Type::INTERFACE:
//             cerr << "interface" << endl ;
//             break ;
//         case capnp::schema::Type::ANY_POINTER:
//             cerr << "void*" << endl ;
//             break ;
//         default:
//             cerr << type_names[type.which()] << endl ;
//     }
// }

// Forward declare.
void emitNestedNodes
    ( ostream& os
    , const SchemaLoader& loader
    , const Schema& schema
    , const uint level = 0
    ) ;

void emitUnionEnum
    ( ostream& os
    , const SchemaLoader& loader
    , const Schema& schema
    , const string& indent
    )
{
    const auto union_fields = schema.asStruct().getUnionFields() ;
    if (union_fields.size() > 0) {
        os << indent << "pub const CapnpUnionWhich = enum(u16) {" << endl ;
        uint i = 0 ;
        for (const auto& field: union_fields) {
            os << indent << tab << field.getProto().getName().cStr() << " = " << std::to_string(i) << "," << endl ;
            ++ i ;
        }
        os << indent << "} ;" << endl ;
    }
}

void emitUnionFunctions
    ( ostream& os
    , const SchemaLoader& loader
    , const StructSchema& schema
    , const string& indent
    , const char* // name
    )
{
    const auto union_fields = schema.asStruct().getUnionFields() ;
    if (union_fields.size() > 0) {
        uint i = 0 ;
        os << indent << tab << "pub fn which(self: @This()) @This().CapnpUnionWhich {" << endl ;
        os << indent << tab << tab << "const bytes = self.message.segments.items[self.segment].items[self.data..self.ptrs] ;" << endl ;
        os << indent << tab << tab << "const discriminant = capnp.serialization.get(u16, bytes, " << schema.getProto().getStruct().getDiscriminantOffset() * 2 << ") ;" << endl ;
        os << indent << tab << tab << "return @intToEnum(@This().CapnpUnionWhich, discriminant) ;" << endl ;
        os << indent << tab << "}" << endl ;
        os << indent << tab << "fn setUnion(self: *@This(), val: u16) !void {" << endl ;
        os << indent << tab << tab << "const bytes = self.message.segments.items[self.segment].items[self.data..self.ptrs] ;" << endl ;
        os << indent << tab << tab << "return capnp.serialization.set(u16, val, bytes, " << schema.getProto().getStruct().getDiscriminantOffset() * 2 << ") ;" << endl ;
        os << indent << tab << "}" << endl ;
        for (const auto& field: union_fields) {
            const auto enumname = field.getProto().getName().cStr() ;
            const auto name = uppercaseFirstLetter(enumname) ;
            os << indent << tab << "pub fn is" << name << "(self: @This()) bool {" << endl ;
            os << indent << tab << tab << "return self.which() == @This().CapnpUnionWhich." << enumname << " ;" << endl ;
            os << indent << tab << "}" << endl ;
            ++ i ;
        }
    }
}

///
/// Emit a basic prologue to the zig file.
///
void emitPrologue(ostream& os)
{
    os << "const std   = @import(\"std\") ;" << endl ;
    os << "const capnp = @import(\"capnp\") ;" << endl ;
    os << endl ;
    os << "const bytes_per_word = capnp.bytes_per_word ;" << endl ;
    os << endl ;
    //os << "const Array     = std.ArrayList ;" << endl ;
    //os << endl ;
}

void emitBytesArray(ostream& os, const uint8_t* arr, int len)
{
    os << tab << "&[_]u8 {" ;
    int i = 0 ;
    for (; i < len - 1; ++ i)
        os << std::dec << (uint) arr[i] << "," ;
    os << std::dec << (uint) arr[i] ;
    os << "}," << endl ;
}

///
/// Emit the sequence of bytes corresponding to a Value. Let's be
/// honest, I dislike this implementation.
///
void emitDefaultValue(ostream& os, const capnp::schema::Value::Reader v)
{
    if (v.isBool()) {
        const bool value = v.getBool() ;
        emitBytesArray(os, (const uint8_t*) &value, 1) ;
    } else if (v.isInt8()) { 
        const int8_t value = v.getInt8() ;
        emitBytesArray(os, (const uint8_t*) &value, 1) ;
    } else if (v.isInt16()) { 
        const int16_t value = v.getInt16() ;
        emitBytesArray(os, (const uint8_t*) &value, 2) ;
    } else if (v.isInt32()) { 
        const int32_t value = v.getInt32() ;
        emitBytesArray(os, (const uint8_t*) &value, 4) ;
    } else if (v.isInt64()) { 
        const int64_t value = v.getInt64() ;
        emitBytesArray(os, (const uint8_t*) &value, 8) ;
    } else if (v.isUint8()) { 
        const uint8_t value = v.getUint8() ;
        emitBytesArray(os, (const uint8_t*) &value, 1) ;
    } else if (v.isUint16()) { 
        const uint16_t value = v.getUint16() ;
        emitBytesArray(os, (const uint8_t*) &value, 2) ;
    } else if (v.isUint32()) { 
        const uint32_t value = v.getUint32() ;
        emitBytesArray(os, (const uint8_t*) &value, 4) ;
    } else if (v.isUint64()) { 
        const uint64_t value = v.getUint64() ;
        emitBytesArray(os, (const uint8_t*) &value, 8) ;
    } else if (v.isFloat32()) { 
        const float value = v.getFloat32() ;
        emitBytesArray(os, (const uint8_t*) &value, 4) ;
    } else if (v.isFloat64()) {
        const double value = v.getFloat64() ;
        emitBytesArray(os, (const uint8_t*) &value, 8) ;
    } else if (v.isText()) {
        const capnp::Text::Reader value = v.getText() ;
        os << tab << "// Text of length " << value.size() << endl ;
        //emitBytesArray(os, )
    } else if (v.isData()) {
        const capnp::Data::Reader value = v.getData() ;
        os << tab << "// Data of length " << value.size() << endl ;
        //emitBytesArray(os, )
    } else if (v.isList()) {
    } else if (v.isEnum()) {
        const uint16_t value = v.getEnum() ;
        emitBytesArray(os, (const uint8_t*) &value, 2) ;
    } else if (v.isStruct()) {
    } else if (v.isInterface()) {
    } else if (v.isAnyPointer()) {
    }
}

///
/// Emit an array of arrays containing default values.
///
void emitDefaultValuesArray
    ( ostream& os
    , const SchemaLoader& loader
    , const Schema& topmost_schema
    )
{
    next_field_index = 0 ;
    fields_indices.clear() ;
    os << "const defaults = [_][]const u8 {" << endl ;
    for (const auto& nested: topmost_schema.getProto().getNestedNodes()) {
        const auto name = nested.getName().cStr() ;
        const auto schema = loader.getUnbound(nested.getId()) ;
        const auto proto = schema.getProto() ;
        os << tab << "// " << name << endl ;
        if (proto.isStruct()) {
            for (const auto& field: schema.asStruct().getFields()) {
                // Register the field hashcode in a contiguous set of
                // indices.
                const uint hash = field.hashCode() ;
                fields_indices.insert({hash, next_field_index}) ;
                os << tab << "// " << hash << " -> " << fields_indices[hash] << endl ;
                // Emit the array depending on the type of the field.
                const auto fproto = field.getProto() ;
                if (field.getType().isStruct()) {
                } else if (field.getType().isEnum()) {
                } else if (field.getType().isInterface()) {
                } else {
                    emitDefaultValue(os, fproto.getSlot().getDefaultValue()) ;
                    next_field_index ++ ;
                }
            }
        } else {
            os << tab << "// WTF ?!" << endl ;
        }
    }
    os << "} ;" << endl << endl ;
}

namespace reader {

// void emitFieldGetter
//     ( ostream& os
//     , const SchemaLoader& loader
//     , const capnp::StructSchema::Field& field
//     , const string& indent
//     )
// {
//     const auto primitive = typeIsPrimitive(field.getType()) ;
//     const auto proto = field.getProto() ;
//     const auto name = uppercaseFirstLetter(proto.getName().cStr()) ;
//     const auto type = showType(field.getType()) + (primitive ? "" : "View") ;
//     auto pos_expr = string("capnp.bytes_per_word * (self.struct_view.content_pos") ;
//     if (primitive) {
//         pos_expr += ") + @sizeOf(" + type + ") * " + std::to_string(proto.getSlot().getOffset()) ;
//     } else {
//         const auto parent_proto = field.getContainingStruct().getProto() ;
//         pos_expr += " + " + std::to_string(parent_proto.getStruct().getDataWordCount()) ;
//         pos_expr += ") + @sizeOf(" + type + ") * " + std::to_string(proto.getSlot().getOffset()) ;
//     }
//     os << indent << "pub fn get" << name << "(self: @This(), message: *capnp.MessageStream) !" << type << " {" << endl ;
//     os << indent << tab << "const pos = " << pos_expr << " ;" << endl ;
//     os << indent << tab << "return capnp.deserialization.getInternal(" << type << ", message.*.stream, pos) ;" << endl ;
//     os << indent << "}" << endl ;
// }

// ///
// /// Essentially the same as emitStructFunctions, but without an init function.
// ///
// void emitGroupFunctions
//     ( ostream& os
//     , const SchemaLoader& loader
//     , const StructSchema& schema
//     , const string& indent
//     , const char* name
//     )
// {
//     assert(schema.getProto().getStruct().getIsGroup()) ;
//     for (const auto& field: schema.getFields()) {
//         const auto fproto = field.getProto() ;
//         if (fproto.which() == Field::SLOT) {
//             emitFieldGetter(os, loader, field, indent + tab) ;
//         } else if (fproto.which() == Field::GROUP) {
//             // FIXME See code in the writer.
//         }
//     }
// }

// ///
// /// ...
// ///
// void emitStructFunctions
//     ( ostream& os
//     , const SchemaLoader& loader
//     , const StructSchema& schema
//     , const string& indent
//     , const char* name
//     )
// {
//     /*
//     os << indent << tab << "pub fn init(message: *capnp.MessageStream) !Self {" << endl ;
//     os << indent << tab << tab << "return Self" << endl ;
//     os << indent << tab << tab << tab << "{ .struct_view = try StructView.init(message.*.stream)" << endl ;
//     os << indent << tab << tab << tab << "} ;" << endl ;
//     os << indent << tab << "}" << endl ;
//     for (const auto& field: schema.getFields()) {
//         const auto fproto = field.getProto() ;
//         if (fproto.which() == Field::SLOT) {
//             emitFieldGetter(os, field, indent + tab) ;
//         } else if (fproto.which() == Field::GROUP) {
//             const string name = uppercaseFirstLetter(fproto.getName().cStr()) ;
//             os << indent << tab << "pub const " << name << " = struct {" << endl ;
//             emitGroupFunctions(os, field.getType().asStruct(), indent + tab, name.c_str()) ;
//             os << indent << tab << "} ;" << endl ;
//         }
//     }
//     */
// }

} // end namespace reader

namespace writer {

// void emitListInit
//     ( ostream& os
//     , const capnp::StructSchema::Field& field
//     , const string& indent
//     )
// {
//     const auto proto = field.getProto() ;
//     const auto name = uppercaseFirstLetter(proto.getName().cStr()) ;
//     const auto type = showType(field.getType()) ;
//     string subtype = showType(field.getType().asList().getElementType()) ;
//     string size_expr = "" ;
//     if (field.getType().asList().getElementType().isStruct()) {
//         subtype = "capnp.serialization." + subtype ;
//         // Nice. Really, this looks nice (no). Some day I'll find the
//         // motivation to actually try to understand the OO mess that
//         // capnp is and fix my code. Some day.
//         size_expr += to_string(field.getType().asList().getElementType().asStruct().getProto().getStruct().getDataWordCount()) + " * bytes_per_word," +
//                      to_string(field.getType().asList().getElementType().asStruct().getProto().getStruct().getPointerCount()) + " * bytes_per_word" ;
//     } else {
//         size_expr += "@sizeOf(" + subtype + "), 0" ;
//     }
//     const auto full_type = "capnp.serialization." + type + "(" + subtype + ").Stencil" ;
//     os << indent << "pub fn init" << name << "(self: @This(), n: u32) !" << full_type << " {" << endl ;
//     os << indent << tab << "if (n > capnp.serialization.max_list_len) {" << endl ;
//     os << indent << tab << tab << "return capnp.serialization.Error.ListTooLong ;" << endl ;
//     os << indent << tab << "}" << endl ;
//     os << indent << tab << "const ptr_pos = self.ptrs + " << proto.getSlot().getOffset() << " * bytes_per_word ;" << endl ;
//     os << indent << tab << "return try " << full_type << ".initCompositeFrom(self.message, self.segment, ptr_pos, " << size_expr << ", n) ;" << endl ;
//     os << indent << "}" << endl ;
// }

// void emitListOfListInit
//     ( ostream& os
//     , const capnp::StructSchema::Field& field
//     , const string& indent
//     )
// {
//     const auto proto = field.getProto() ;
//     const auto name = uppercaseFirstLetter(proto.getName().cStr()) ;
//     const auto type = showType(field.getType()) ;
//     const auto subtype = showType(field.getType().asList().getElementType().asList().getElementType()) ;
//     const auto full_type = "capnp.serialization." + type + "(" + subtype + ").Stencil" ;
//     os << indent << "pub fn init" << name << "(self: @This(), n: u32) !" << full_type << " {" << endl ;
//     os << indent << tab << "if (n > capnp.serialization.max_list_len) {" << endl ;
//     os << indent << tab << tab << "return capnp.serialization.Error.ListTooLong ;" << endl ;
//     os << indent << tab << "}" << endl ;
//     os << indent << tab << "var bytes_for_ptr = self.struct_stencil.ptr[" ;
//     os << proto.getSlot().getOffset() << "*bytes_per_word.." << proto.getSlot().getOffset()+1 << "*bytes_per_word] ;" << endl ;
//     os << indent << tab << "return try " << full_type << ".init(n, @sizeOf(" << subtype << "), bytes_for_ptr, self.message) ;" << endl ;
//     os << indent << "}" << endl ;
// }

// /*
// void emitListOfStructInit
//     ( ostream& os
//     , const capnp::StructSchema::Field& field
//     , const string& indent
//     )
// {
//     const auto proto = field.getProto() ;
//     const auto name = uppercaseFirstLetter(proto.getName().cStr()) ;
//     const auto type = showType(field.getType()) ;
//     // This should NOT happen (only in emitListOfListInit)...
//     const bool is_list_of_list = (type == type_names[field.getType().which() + 5]) ;
//     const auto subtype = is_list_of_list
//         ? showType(field.getType().asList().getElementType().asList().getElementType())
//         : showType(field.getType().asList().getElementType()) ;
//     const auto full_type = type + "(" + subtype + ").Stencil" ;
//     os << indent << "pub fn init" << name << "(self: Self, n: u32) !" << full_type << " {" << endl ;
//     os << indent << tab << "if (n > capnp.serialization.max_list_len) {" << endl ;
//     os << indent << tab << tab << "return capnp.serialization.Error.ListTooLong ;" << endl ;
//     os << indent << tab << "}" << endl ;
//     os << indent << tab << "var bytes_for_ptr = self.struct_stencil.ptr[" ;
//     os << proto.getSlot().getOffset() << "*bytes_per_word.." << proto.getSlot().getOffset()+1 << "*bytes_per_word] ;" << endl ;
//     os << indent << tab << "return try " << full_type << ".init(n, @sizeOf(" << subtype << "), bytes_for_ptr, self.message) ;" << endl ;
//     os << indent << "}" << endl ;
// }
// */

// /*
// void emitStructInit
//     ( ostream& os
//     , const capnp::StructSchema::Field& field
//     , const string& indent
//     )
// {
// }
// */

// void emitPrimitiveSet
//     ( ostream& os
//     , const SchemaLoader& loader
//     , const capnp::StructSchema::Field& field
//     , const string& indent
//     )
// {
//     const auto proto = field.getProto() ;
//     const auto name = uppercaseFirstLetter(proto.getName().cStr()) ;
//     const auto type = showType(field.getType()) ;
//     const auto pos_expr = "@sizeOf(" + type + ") * " + std::to_string(proto.getSlot().getOffset()) ;
//     os << indent << "pub fn set" << name << "(self: *@This(), value: " << type << ") !void {" << endl ;
//     os << indent << tab << "var bytes = self.message.segments.items[self.segment].items[self.data..self.ptrs] ;" << endl ;
//     // If this is an union, set the union enum.
//     const auto discriminant = field.getProto().getDiscriminantValue() ;
//     if (discriminant != 0xffff) { // FIXME Find a way to access Field.noDiscriminant.
//         os << indent << tab << "try self.setUnion(" << discriminant << ") ;" << endl ;
//     }
//     // Compute the offset.
//     if (field.getType() == schema::Type::BOOL) {
//         os << indent << tab << "const offset = " << std::to_string(proto.getSlot().getOffset()) << " ;" << endl ;
//     } else {
//         os << indent << tab << "const offset = @sizeOf(" << type << ") * " << std::to_string(proto.getSlot().getOffset()) << " ;" << endl ;
//     }
//     // Get the value and cast appropriately from memory.
//     if (field.getType().which() == schema::Type::Which::ENUM) {
//         os << indent << tab << "return capnp.serialization.set(u16, @enumToInt(value), bytes, offset) ;" << endl ;
//     } else {
//         os << indent << tab << "return capnp.serialization.set(" << type << ", value, bytes, offset) ;" << endl ;
//     }
//     os << indent << "}" << endl ;
// }

// void emitPrimitiveGet
//     ( ostream& os
//     , const SchemaLoader& loader
//     , const capnp::StructSchema::Field& field
//     , const string& indent
//     )
// {
//     const auto proto = field.getProto() ;
//     const auto name = uppercaseFirstLetter(proto.getName().cStr()) ;
//     const auto type = showType(field.getType()) ;
//     os << indent << "pub fn get" << name << "(self: @This()) !" << type << "{" << endl ;
//     os << indent << tab << "var bytes = self.message.segments.items[self.segment].items[self.data..self.ptrs] ;" << endl ;
//     if (field.getType() == schema::Type::BOOL) {
//         os << indent << tab << "const offset = " << std::to_string(proto.getSlot().getOffset()) << " ;" << endl ;
//     } else {
//         os << indent << tab << "const offset = @sizeOf(" << type << ") * " << std::to_string(proto.getSlot().getOffset()) << " ;" << endl ;
//     }
//     if (field.getType().which() == schema::Type::Which::ENUM) {
//         os << indent << tab << "return @intToEnum(" << type << ", try capnp.serialization.get(u16, bytes, offset)) ;" << endl ;
//     } else {
//         os << indent << tab << "return capnp.serialization.get(" << type << ", bytes, offset) ;" << endl ;
//     }
//     os << indent << "}" << endl ;
// }

// void emitPrimitiveFunctions
//     ( ostream& os
//     , const SchemaLoader& loader
//     , const capnp::StructSchema::Field& field
//     , const string& indent
//     )
// {
//     emitPrimitiveSet(os, loader, field, indent) ;
//     emitPrimitiveGet(os, loader, field, indent) ;
// }

// void emitMemberStructInit
//     ( ostream& os
//     , const SchemaLoader& loader
//     , const capnp::StructSchema::Field& field
//     , const string& indent
//     )
// {
//     const auto proto = field.getProto() ;
//     const auto name = uppercaseFirstLetter(proto.getName().cStr()) ;
//     const auto type = nodeName(field.getType().asStruct()) ;
//     os << indent << "pub fn init" << name << "(self: @This()) !" << type << ".Stencil {" << endl ;
//     os << indent << tab << "const pos: u32 = self.ptrs + " << proto.getSlot().getOffset() << " * bytes_per_word ;" << endl ;
//     os << indent << tab << "return " << type << ".Stencil.initFrom(self.message, self.segment, pos) ;" << endl ;
//     os << indent << "}" << endl ;
// }

// void emitMemberStructSet
//     ( ostream& os
//     , const SchemaLoader& loader
//     , const capnp::StructSchema::Field& field
//     , const string& indent
//     )
// {
//     cerr << indent << "// set :: Stencil -> ()" << endl ;
//     /*
//     const auto proto = field.getProto() ;
//     const auto parent_proto = field.getContainingStruct().getProto() ;
//     const auto name = uppercaseFirstLetter(proto.getName().cStr()) ;
//     const auto type = nodeName(field.getType().asStruct()) ;
//     const auto pos_expr = string("capnp.bytes_per_word * ((self.struct_view.content_pos) + ") +
//             std::to_string(parent_proto.getStruct().getDataWordCount()) +
//             ") + @sizeOf(" + type + ") * " +
//             std::to_string(proto.getSlot().getOffset()) ;
//     os << indent << "pub fn set" << name << "(self: @This(), source: " << type << ".View) !void {" << endl ;
//     os << indent << tab << "const pos = " << pos_expr << " ;" << endl ;
//     os << indent << tab << "return capnp.serialization.setInternal(" << type << ", source, self.struct_stencil.data, pos) ;" << endl ;
//     os << indent << "}" << endl ;
//     */
// }

// void emitMemberStructGet
//     ( ostream& os
//     , const SchemaLoader& loader
//     , const capnp::StructSchema::Field& field
//     , const string& indent
//     )
// {
//     cerr << indent << "// get :: () -> Stencil" << endl ;
//     const auto proto = field.getProto() ;
//     const auto parent_proto = field.getContainingStruct().getProto() ;
//     const auto name = uppercaseFirstLetter(proto.getName().cStr()) ;
//     const auto type = nodeName(field.getType().asStruct()) ;
//     const auto pos_expr = string("capnp.bytes_per_word * ((self.struct_view.content_pos) + ") +
//             std::to_string(parent_proto.getStruct().getDataWordCount()) +
//             ") + @sizeOf(" + type + ") * " +
//             std::to_string(proto.getSlot().getOffset()) ;
//     os << indent << "pub fn get" << name << "(self: @This()) !" << type << ".Stencil {" << endl ;
//     os << indent << tab << "const pos = " << pos_expr << " ;" << endl ;
//     os << indent << tab << "return capnp.serialization.getInternal(" << type << ", self.struct_stencil.data, pos) ;" << endl ;
//     os << indent << "}" << endl ;
// }

// void emitMemberStructHas
//     ( ostream& os
//     , const SchemaLoader& loader
//     , const capnp::StructSchema::Field& field
//     , const string& indent
//     )
// {
//     const auto proto = field.getProto() ;
//     const auto name = uppercaseFirstLetter(proto.getName().cStr()) ;
//     os << indent << "pub fn has" << name << "(self: @This()) !bool {" << endl ;
//     os << indent << tab << "_ = self ;" << endl ;
//     os << indent << tab << "// TODO." << endl ;
//     os << indent << "}" << endl ;
// }

// void emitMemberStructAdopt
//     ( ostream& os
//     , const SchemaLoader& loader
//     , const capnp::StructSchema::Field& field
//     , const string& indent
//     )
// {
//     cerr << indent << "// adopt (TODO, ORPHANS)." << endl ;
// }

// void emitMemberStructDisown
//     ( ostream& os
//     , const SchemaLoader& loader
//     , const capnp::StructSchema::Field& field
//     , const string& indent
//     )
// {
//     cerr << indent << "// disown (TODO, ORPHANS)." << endl ;
// }

// void emitMemberStructFunctions
//     ( ostream& os
//     , const SchemaLoader& loader
//     , const capnp::StructSchema::Field& field
//     , const string& indent
//     )
// {
//     emitMemberStructInit(os, loader, field, indent) ;
//     //emitMemberStructSet(os, loader, field, indent) ;
//     //emitMemberStructGet(os, loader, field, indent) ;
//     //emitMemberStructHas(os, loader, field, indent) ;
//     //emitMemberStructAdopt(os, loader, field, indent) ;
//     //emitMemberStructDisown(os, loader, field, indent) ;
// }

// void emitListOfPrimitivesFunctions
//     ( ostream& os
//     , const SchemaLoader& loader
//     , const capnp::StructSchema::Field& field
//     , const string& indent
//     )
// {
//     emitListInit(os, field, indent) ;
//     //emitListSet() ;
//     //emitListGet() ;
//     cerr << endl << "// TODO List functions..." << endl << endl ;
// }

// void emitListOfListsFunctions
//     ( ostream& os
//     , const SchemaLoader& loader
//     , const capnp::StructSchema::Field& field
//     , const string& indent
//     )
// {
//     emitListOfListInit(os, field, indent) ;
//     cerr << endl << "// TODO ListOfList functions..." << endl << endl ;
// }

// void emitListOfStructsFunctions
//     ( ostream& os
//     , const SchemaLoader& loader
//     , const capnp::StructSchema::Field& field
//     , const string& indent
//     )
// {
//     emitListInit(os, field, indent) ;
//     cerr << endl << "// TODO ListOfStructs functions..." << endl << endl ;
// }

// void emitListOfPointersFunctions
//     ( ostream& os
//     , const SchemaLoader& loader
//     , const capnp::StructSchema::Field& field
//     , const string& indent
//     )
// {
//     cerr << endl << "// TODO ListOfPointers functions..." << endl << endl ;
// }

// void emitFieldFunctions
//     ( ostream& os
//     , const SchemaLoader& loader
//     , const capnp::StructSchema::Field& field
//     , const string& indent
//     )
// {
//     if (field.getType().isList()) {
//         if (field.getType().asList().getElementType().isList()) {
//             emitListOfListsFunctions(os, loader, field, indent) ;
//             emitListOfPointersFunctions(os, loader, field, indent) ; // TODO
//         } else if (field.getType().asList().getElementType().isStruct()) {
//             emitListOfStructsFunctions(os, loader, field, indent) ;
//             emitListOfPointersFunctions(os, loader, field, indent) ; // TODO
//         } else {
//             emitListOfPrimitivesFunctions(os, loader, field, indent) ;
//         }
//         //emitPointerFunctions(os, loader, field, indent) ; // TODO Should we ?
//     } else if (field.getType().isStruct()) {
//         emitMemberStructFunctions(os, loader, field, indent) ;
//         //emitPointerFunctions(os, loader, field, indent) ;
//     } else {
//         emitPrimitiveFunctions(os, loader, field, indent) ;
//     }
// }


void emitDiscriminantChange
    ( ostream& os
    , const string& indent
    , const uint discriminant_count = 0
    )
{
    if (discriminant_count > 0) {
        os << indent << "// CHANGE DISCRIMINANT HERE" << endl ;
    }
}


void emitSlotGetFunction
    ( ostream& os
    , const SchemaLoader& loader
    , const capnp::StructSchema::Field& field
    , const TypeSummary& summary
    , const string& indent
    )
{
    const uint hash = field.hashCode() ;
    const uint fidx = fields_indices[hash] ;
    const auto proto = field.getProto() ;
    const auto name = uppercaseFirstLetter(proto.getName().cStr()) ;
    const auto type = showType(field.getType(), ser_prefix) ;
    if (summary.is_primitive) {
        assert(not summary.is_struct) ;
        assert(not summary.is_pointer) ;
        assert(not summary.is_list) ;
        assert(not summary.is_data) ;
        assert(not summary.is_text) ;
        os << indent << "// GET PRIMITIVE" << endl ;
        {
            const bool is_enum = field.getType().isEnum() ;
            const bool is_bool = field.getType().isBool() ;
            string bits_offset_expr = "0" ;
            string bytes_offset_expr = "0" ;
            if (is_bool) {
                bits_offset_expr = "@mod(" + std::to_string(proto.getSlot().getOffset()) + ", 8)" ;
                bytes_offset_expr = "self.data + @divTrunc(" + std::to_string(proto.getSlot().getOffset()) + ", 8)" ;
            } else {
                bytes_offset_expr = string("self.data + @sizeOf(") + type +
                    ") * " + std::to_string(proto.getSlot().getOffset()) ;
            }
            os << indent << "pub fn get" << name << "(self: @This()) !" << type << " {" << endl ;
            os << indent << tab << "const bytes_offset = " << bytes_offset_expr << " ;" << endl ;
            os << indent << tab << "const bits_offset = " << bits_offset_expr << " ;" << endl ;
            os << indent << tab << "var bytes = try self.message.getSlice(self.segment, bytes_offset, @sizeOf(" << type << ")) ;" << endl ;
            if (is_enum)
                os << indent << tab << "return @intToEnum(" << type << ", capnp.serialization.getInternal(u16, bytes, defaults[" << fidx << "], bits_offset)) ;" << endl ;
            else
                os << indent << tab << "return capnp.serialization.getInternal(" << type << ", bytes, defaults[" << fidx << "], bits_offset) ;" << endl ;
            os << indent << "}" << endl ;
        }
    } else if (summary.is_list) {
        os << indent << "// GET LIST" << endl ;
        os << indent << "pub fn get" << name << "(self: @This()) !" << type << ".Stencil {" << endl ;
        os << indent << tab << "return " << type << ".Stencil.initFromPointer(self.message, self.segment, " ;
        os << "self.ptrs + capnp.bytes_per_word * " << proto.getSlot().getOffset() << ") ;" << endl ;
        os << indent << "}" << endl ;
    } else if (summary.is_data or summary.is_text) {
        os << indent << "// GET DATA OR TEXT" << endl ;
        os << indent << "pub fn get" << name << "(self: @This()) !" << type << ".Stencil {" << endl ;
        os << indent << tab << "return " << type << ".Stencil.initFromPointer(self.message, self.segment, " ;
        os << "self.ptrs + capnp.bytes_per_word * " << proto.getSlot().getOffset() << ") ;" << endl ;
        os << indent << "}" << endl ;
    } else if (summary.is_pointer) {
        os << indent << "// GET COMPOSITE (POINTER)" << endl ;
    }
}


void emitSlotSetFunction
    ( ostream& os
    , const SchemaLoader& loader
    , const capnp::StructSchema::Field& field
    , const TypeSummary& summary
    , const string& indent
    )
{
    const uint hash = field.hashCode() ;
    const uint fidx = fields_indices[hash] ;
    const auto proto = field.getProto() ;
    const auto name = uppercaseFirstLetter(proto.getName().cStr()) ;
    const auto type = showType(field.getType(), ser_prefix) ;
    if (summary.is_primitive) {
        assert(not summary.is_struct) ;
        assert(not summary.is_pointer) ;
        assert(not summary.is_list) ;
        assert(not summary.is_data) ;
        assert(not summary.is_text) ;
        os << indent << "// SET PRIMITIVE" << endl ;
        {
            const bool is_enum = field.getType().isEnum() ;
            const bool is_bool = field.getType().isBool() ;
            string bits_offset_expr = "0" ;
            string bytes_offset_expr = "0" ;
            if (is_bool) {
                bits_offset_expr = "@mod(" + std::to_string(proto.getSlot().getOffset()) + ", 8)" ;
                bytes_offset_expr = "self.data + @divTrunc(" + std::to_string(proto.getSlot().getOffset()) + ", 8)" ;
            } else {
                bytes_offset_expr = string("self.data + @sizeOf(") + type +
                    ") * " + std::to_string(proto.getSlot().getOffset()) ;
            }
            os << indent << "pub fn set" << name << "(self: @This(), source: " << type << ") !void {" << endl ;
            os << indent << tab << "const bytes_offset = " << bytes_offset_expr << " ;" << endl ;
            os << indent << tab << "const bits_offset = " << bits_offset_expr << " ;" << endl ;
            os << indent << tab << "var bytes = try self.message.getSlice(self.segment, bytes_offset, @sizeOf(" << type << ")) ; // WHAT THE DUCK ?!" << endl ;
            if (is_enum)
                os << indent << tab << "return capnp.serialization.setInternal(u16, @enumToInt(source), bytes, defaults[" << fidx << "], bits_offset) ;" << endl ;
            else
                os << indent << tab << "return capnp.serialization.setInternal(" << type << ", source, bytes, defaults[" << fidx << "], bits_offset) ;" << endl ;
            os << indent << "}" << endl ;
        }
    } else if (summary.is_list or summary.is_data or summary.is_text) {
        os << indent << "// SET LIST, DATA OR TEXT" << endl ;
        os << indent << "pub fn set" << name << "(self: @This(), src: " << type << ".Stencil) !void {" << endl ;
        os << indent << tab << "_ = self ;" << endl ;
        os << indent << tab << "_ = src ;" << endl ;
        os << indent << "}" << endl ;
    } else if (summary.is_pointer) {
        os << indent << "// SET POINTER (COMPOSITE)" << endl ;
        emitDiscriminantChange(os, indent) ;
    }
}


void emitSlotHasFunction
    ( ostream& os
    , const SchemaLoader& loader
    , const capnp::StructSchema::Field& field
    , const TypeSummary& summary
    , const string& indent
    )
{
    if (summary.is_pointer) {
        assert(not summary.is_primitive) ;
        os << indent << "// HAS POINTER" << endl ;
    }
}


void emitSlotInitFunction
    ( ostream& os
    , const SchemaLoader& loader
    , const capnp::StructSchema::Field& field
    , const TypeSummary& summary
    , const string& indent
    )
{
    const auto proto = field.getProto() ;
    const auto name = uppercaseFirstLetter(proto.getName().cStr()) ;
    const auto type = showType(field.getType(), ser_prefix) ;
    // TODO Branches are extremely similar.
    if (summary.is_list) { // or summary.is_data or summary.is_text) {
        assert(summary.is_pointer) ;
        assert(not summary.is_primitive) ;
        const ListSummary list_summary = summarizeList(field.getType()) ;
        const bool is_list_of_structs = list_summary.element_type.isStruct() ;
        os << indent << "// INIT LIST(N) (OF STRUCTS: " << is_list_of_structs << ")" << endl ;
        os << indent << "pub fn init" << name << "(self: @This(), len: u32) !" << type << ".Stencil {" << endl ;
        if (is_list_of_structs) {
            const auto data_bytes = list_summary.element_type.asStruct().getProto().getStruct().getDataWordCount() * 8 ;
            const auto ptrs_bytes = list_summary.element_type.asStruct().getProto().getStruct().getPointerCount()  * 8 ;
            os << indent << tab << "return " << type << ".Stencil.initCompositeFrom(" ;
            os << "self.message, self.segment, self.ptrs + capnp.bytes_per_word * " ;
            os << proto.getSlot().getOffset() << ", " << data_bytes << ", " << ptrs_bytes << ", len) ;" << endl ;
        } else {
            os << indent << tab << "return " << type << ".Stencil.initFrom(" ;
            os << "self.message, self.segment, self.ptrs + capnp.bytes_per_word * " ;
            os << proto.getSlot().getOffset() << ", @sizeOf(" << showType(field.getType().asList().getElementType(), ser_prefix) << "), len) ;" << endl ;
        }
        os << indent << "}" << endl ;
        emitDiscriminantChange(os, indent, field.getContainingStruct().getProto().getStruct().getDiscriminantCount()) ;
    } else if (summary.is_data or summary.is_text) {
        os << indent << "// INIT DATA OR TEXT" << endl ;
        os << indent << "pub fn init" << name << "With(self: @This(), data: []const u8) !" << type << ".Stencil {" << endl ;
        os << indent << tab << "if (data.len > 10000) { // FIXME" << endl ;
        os << indent << tab << tab << "return capnp.serialization.Error.OutOfBounds ; // FIXME Different error..." << endl ;
        os << indent << tab << "}" << endl ;
        os << indent << tab << "const stencil = try " << type << ".Stencil.initFrom(" ;
        os << "self.message, self.segment, self.ptrs + capnp.bytes_per_word * " ;
        os << proto.getSlot().getOffset() << ", @intCast(u32, data.len)) ;" << endl ;
        os << indent << tab << "try stencil.setSlice(0, data) ;" << endl ;
        os << indent << tab << "return stencil ;" << endl ;
        os << indent << "}" << endl ;
    } else if (summary.is_struct) {
        assert(summary.is_pointer) ;
        assert(not summary.is_primitive) ;
        os << indent << "// INIT STRUCT" << endl ;
        os << indent << "pub fn init" << name << "(self: @This()) !" << type << ".Stencil {" << endl ;
        os << indent << tab << "return " << type << ".Stencil.initFrom(" ;
        os << "self.message, self.segment, self.ptrs + capnp.bytes_per_word * " ;
        os << proto.getSlot().getOffset() << ") ;" << endl ;
        os << indent << "}" << endl ;
        emitDiscriminantChange(os, indent, field.getContainingStruct().getProto().getStruct().getDiscriminantCount()) ;
    }
}


void emitSlotAdoptFunction
    ( ostream& os
    , const SchemaLoader& loader
    , const capnp::StructSchema::Field& field
    , const TypeSummary& summary
    , const string& indent
    )
{
    if (summary.is_pointer) {
        assert(not summary.is_primitive) ;
        os << indent << "// ADOPT POINTER" << endl ;
        emitDiscriminantChange(os, indent) ;
    }
}


void emitSlotDisownFunction
    ( ostream& os
    , const SchemaLoader& loader
    , const capnp::StructSchema::Field& field
    , const TypeSummary& summary
    , const string& indent
    )
{
    if (summary.is_pointer) {
        assert(not summary.is_primitive) ;
        os << indent << "// DISOWN POINTER" << endl ;
    }
}


void emitSlotFunctions
    ( ostream& os
    , const SchemaLoader& loader
    , const capnp::StructSchema::Field& field
    , const string& indent
    )
{
    // Compute a summary of traits of the field type.
    TypeSummary summary = summarizeType(field.getType()) ;
    // Emit functions (callees will decide what to emit or not).
    emitSlotGetFunction(   os, loader, field, summary, indent) ;
    emitSlotSetFunction(   os, loader, field, summary, indent) ;
    emitSlotHasFunction(   os, loader, field, summary, indent) ;
    emitSlotInitFunction(  os, loader, field, summary, indent) ;
    emitSlotAdoptFunction( os, loader, field, summary, indent) ;
    emitSlotDisownFunction(os, loader, field, summary, indent) ;
}


///
/// Emit the function of structs that determines their size, allocate it on
/// the buffer, and initialize the members.
///
void emitStructInitFromFunction
    ( ostream& os
    , const SchemaLoader&
    , const StructSchema& schema
    , const string& indent
    , const char* name
    )
{
    const auto proto = schema.getProto() ;
    os << indent << tab << "pub fn initFrom(buffer: *capnp.MessageBuffer, ptr_seg: u32, ptr_pos: u32) !@This() {" << endl ;
    const auto data_wc = proto.getStruct().getDataWordCount() ;
    const auto ptrs_wc = proto.getStruct().getPointerCount() ;
    // Ask the buffer to allocate some space.
    os << indent << tab << tab << "const ptr = try buffer.alloc(" ;
    os << (data_wc + ptrs_wc) << " * bytes_per_word, ptr_seg, ptr_pos) ;" << endl ;
    // Get some useful constants.
    os << indent << tab << tab << "const s = ptr.data_segment ;" << endl ;
    os << indent << tab << tab << "const p = ptr.data_pos ;" << endl ;
    os << indent << tab << tab << "const data_section_size = @intCast(u32, " << data_wc << " * bytes_per_word) ;" << endl ;
    os << indent << tab << tab << "var data_section = buffer.segments.items[s].items[p..p + data_section_size] ;" << endl ;
    os << indent << tab << tab << "const q = p + data_section_size ;" << endl ;
    os << indent << tab << tab << "const ptrs_section_size = " << ptrs_wc << " * bytes_per_word ;" << endl ;
    os << indent << tab << tab << "var ptrs_section = buffer.segments.items[s].items[q..q + ptrs_section_size] ;" << endl ;
    // Encode the pointer.
    //os << indent << tab << tab << "std.debug.print(\"(s: {}, p: {}, q: {})\\n\", .{s, p, q}) ;" << endl ;
    //os << indent << tab << tab << "std.debug.print(\"({}, {})\\n\", .{ptr.ptr_segment, ptr.ptr_pos}) ;" << endl ;
    os << indent << tab << tab << "var ptr_word = buffer.segments.items[ptr.ptr_segment].items[ptr.ptr_pos..][0..bytes_per_word] ;" << endl ;
    os << indent << tab << tab << "capnp.serialization.encodeStructPointer((p - ptr_pos - 1)/bytes_per_word, data_section_size/bytes_per_word, ptrs_section_size/bytes_per_word, ptr_word) ;" << endl ;
    // Initialize members to their default values.
    // TODO This is a complex topic.
    os << indent << tab << tab << "for (data_section) |*byte| { byte.* = 0 ; }" << endl ;
    /*
    for (const auto& field: schema.getFields()) {
        const auto fproto = field.getProto() ;
        if (fproto.which() == Field::SLOT) {
            // ....
        }
    }
    */
    // Initialize pointers to zero.
    os << indent << tab << tab << "for (ptrs_section) |*byte| { byte.* = 0 ; }" << endl ;
    // Return the stencil and conclude.
    os << indent << tab << tab << "return @This() { .message = buffer, .segment = s, .data = p, .ptrs = q } ;" << endl ;
    os << indent << tab << "}" << endl ;
}

///
/// A small API tweak. TODO Ideally this should be active only for the type
/// of the root struct, but this is not given from the schema itself.
///
void emitStructInitAsRootFunction
    ( ostream& os
    , const SchemaLoader&
    , const StructSchema&
    , const string& indent
    , const char*
    )
{
    os << indent << tab << "pub fn initAsRoot(buffer: *capnp.MessageBuffer) !@This() {" << endl ;
    os << indent << tab << tab << "return initFrom(buffer, 0, 0) ;" << endl ;
    os << indent << tab << "}" << endl ;
}

///
/// Essentially the same as emitStructFunctions, but without an init function.
///
void emitGroupFunctions
    ( ostream& os
    , const SchemaLoader& loader
    , const StructSchema& schema
    , const string& indent
    , const char* name
    )
{
    for (const auto& field: schema.getFields()) {
        const auto fproto = field.getProto() ;
        if (fproto.which() == Field::SLOT) {
            //emitFieldFunctions(os, loader, field, indent + tab) ;
            emitSlotFunctions(os, loader, field, indent + tab) ;
        } else if (fproto.which() == Field::GROUP) {
            const string name = uppercaseFirstLetter(fproto.getName().cStr()) ;
            os << indent << tab << "pub const " << name << " = struct {" << endl ;
            os << indent << tab << tab << "message: *capnp.MessageBuffer," << endl ;
            os << indent << tab << tab << "segment: u32," << endl ;
            os << indent << tab << tab << "data: u32," << endl ;
            os << indent << tab << tab << "ptrs: u32," << endl ;
            //emitUnionEnum(os, loader, field.getType().asStruct(), indent + tab + tab) ;
            //emitGroupFunctions(os, loader, field.getType().asStruct(), indent + tab, name.c_str()) ;
            os << indent << tab << "} ;" << endl ;
            os << indent << tab << "pub fn get" << name << "(self: *@This()) " << name << " {" << endl ;
            os << indent << tab << tab << "return " << name << endl ;
            os << indent << tab << tab << tab << "{ .message = self.message " << endl ;
            os << indent << tab << tab << tab << ", .segment = self.segment " << endl ;
            os << indent << tab << tab << tab << ", .data    = self.data " << endl ;
            os << indent << tab << tab << tab << ", .ptrs    = self.ptrs " << endl ;
            os << indent << tab << tab << tab << "} ;" << endl ;
            os << indent << tab << "}" << endl ;
        }
    }
    //emitUnionFunctions(os, loader, schema, indent, NULL) ;
}

///
/// ...
///
void emitStructFunctions
    ( ostream& os
    , const SchemaLoader& loader
    , const StructSchema& schema
    , const string& indent
    , const char* name
    )
{
    emitStructInitFromFunction(os, loader, schema, indent, name) ;
    emitStructInitAsRootFunction(os, loader, schema, indent, name) ;
    emitGroupFunctions(os, loader, schema, indent, name) ;
}

} // end namespace writer



///
/// Emit a structure as well as its reading / writing functions. This
/// is a recursive function. The passed node is guaranteed to be of
/// type struct.
///
void emitStruct
    ( ostream& os
    , const SchemaLoader& loader
    , const Schema& schema
    , const char* name
    , const uint level
    )
{
    const auto structschema = schema.asStruct() ;
    const auto proto = schema.getProto() ;
    // Some "I don't know what I'm doing" check...
    if (proto.getStruct().getIsGroup()) {
        cerr << "Error, there is a group (" << proto.getDisplayName().cStr() << ") which should *not* appear here." << endl ;
        return ;
    }
    string indent = "" ;
    for (uint i = 0; i < level ; ++ i) {
        indent += tab ;
    }
    string subindent = indent + tab ;
    os << indent << "pub const " << name << " = struct {" << endl ;
    // Emit nested nodes.
    emitNestedNodes(os, loader, schema, level + 1) ;
    // Emit the unnamed union enum if the struct has overlapping fields.
    emitUnionEnum(os, loader, schema, subindent) ;
    /*
    // Reader.
    os << subindent << "pub const " << "View = struct {" << endl ;
    reader::emitStructFunctions(os, loader, structschema, subindent, name) ;
    os << subindent << "} ;" << endl ;
    */
    // Writer.
    os << subindent << "pub const " << "Stencil = struct {" << endl ;
    os << subindent << tab << "message: *capnp.MessageBuffer," << endl ;
    os << subindent << tab << "segment: u32," << endl ;
    os << subindent << tab << "data: u32, // bytes offset in segment" << endl ;
    os << subindent << tab << "ptrs: u32, // bytes offset in segment" << endl ;
    writer::emitStructFunctions(os, loader, structschema, subindent, name) ;
    os << subindent << "} ;" << endl ;
    os << indent << "} ;" << endl ;
    os << endl ;
}

///
/// Emit an enum.
///
void emitEnum
    ( ostream& os
    , const SchemaLoader& loader
    , const Schema& schema
    , const char* name
    , const uint level
    )
{
    string indent = "" ;
    for (uint i = 0; i < level ; ++ i) {
        indent += tab ;
    }
    // Simply emit all enumerants. The correct order (w.r.t the @i index)
    // seems to be handled correctly directly in the AST. No checking here.
    os << indent << "pub const " << name << " = enum(u16) {" << endl ;
    for (const auto& enumerant: schema.asEnum().getEnumerants()) {
        os << indent << tab << enumerant.getProto().getName().cStr() << "," << endl ;
    }
    os << indent << "} ;" << endl ;
    os << endl ;
} ;

///
/// ...
///
void emitNestedNodes
    ( ostream& os
    , const SchemaLoader& loader
    , const Schema& node_schema
    , const uint level
    )
{
    for (const auto& nested: node_schema.getProto().getNestedNodes()) {
        const auto name = nested.getName().cStr() ;
        const auto schema = loader.getUnbound(nested.getId()) ;
        const auto proto = schema.getProto() ;
        if (proto.isStruct()) {
            emitStruct(os, loader, schema, name, level) ;
        } else if (proto.isEnum()) {
            emitEnum(os, loader, schema, name, level) ;
        } else if (proto.isInterface()) {
            cerr << "// Went over an interface node. Skipping..." << endl ;
        } else if (proto.isConst()) {
            cerr << "// Went over an constant node. Skipping..." << endl ;
        } else if (proto.isAnnotation()) {
            cerr << "// Went over an annotation node. Skipping..." << endl ;
        } else {
            cerr << "// Some node is not what it is supposed to be." << endl ;
        }
    }
}

///
/// Emit an epilogue to the zig file.
///
void emitEpilogue(ostream& os) {
    // Nothing.
}

///
/// ...
///
void emitFile
    ( const SchemaLoader& loader
    , const size_t id
    )
{
    const auto schema = loader.get(id) ;
    ostream os(std::cout.rdbuf()) ;
    emitPrologue(os) ;
    emitDefaultValuesArray(os, loader, schema) ;
    emitNestedNodes(os, loader, schema) ;
    emitEpilogue(os) ;
}

///
/// General function emitting the zig file.
///
ErrorCode generateFrom(int fd)
{
    capnp::StreamFdMessageReader message(fd, {}) ;
    auto request = message.getRoot<Request>() ;
    SchemaLoader schema_loader ;
    for (auto node: request.getNodes()) {
        schema_loader.load(node);
    }
    // // Debug...
    // for (auto node: request.getNodes()) {
    //     cerr << "// node with id " << node.getId() << " is " << node.getDisplayName().cStr() << endl ;
    //     for (const auto& nested: node.getNestedNodes()) {
    //         cerr << "//     " << nested.getId() << " is " << nested.getName().cStr() << endl ;
    //     }
    // }
    for (const auto& requested_file: request.getRequestedFiles()) {
        emitFile(schema_loader, requested_file.getId());
    }
    return ErrorCode::Ok ;
}

int main()
{
    return static_cast<int>(generateFrom(STDIN_FILENO)) ;
}
