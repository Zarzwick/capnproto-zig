-- This is a proof of concept of the C++ generator. The call graph is
-- meant to be the same, save for some simplifications tied to types.
-- Here are a few remarks:
--
-- + A struct, appart from init functions, has the same functions as
--   a group. Thus, emitStruct calls emitGroup every time.
-- + Unions are handled as groups, as explained in schema.capnp.

-- All the possible capnp types.
data Type =
      PrimUInt8
    | PrimUInt16
    | PrimUInt32
    | PrimUInt64
    | PrimInt8
    | PrimInt16
    | PrimInt32
    | PrimInt64
    | PrimBool
    | PrimVoid
    | PrimFloat32
    | PrimFloat64
    | Enum
    | Data
    | Text
    | List Type
    | Struct String
    | Interface
    | AnyPtr
    deriving (Show)


-- Fields of a structure.
data Field =
      Slot String Type (Maybe Int)
    | Group String Node
    deriving (Show)


-- The first field is always the list of nested nodes. This is a mere
-- simplification of the way it is done in the capnp schema.
data Node =
    --                                     isGroup discriminant
      StructNode     String [Node] [Field] Bool    Int
    | FileNode       String [Node]
    | EnumNode       String [Node] [String]
    | ConstNode      String [Node] Type
    | InterfaceNode  String [Node]
    | AnnotationNode String [Node]
    deriving (Show)


showDiscriminantChange :: Maybe Int -> String
showDiscriminantChange (Just discr) = " // change to " ++ (show discr)
showDiscriminantChange Nothing = ""


recDepthAndBaseForType :: Type -> (Type, Int)
recDepthAndBaseForType (List t) = let (bt, d) = recDepthAndBaseForType t in (bt, d+1)
recDepthAndBaseForType t = (t, 0)

showType :: Type -> String
showType (List t) = let (bt, d) = recDepthAndBaseForType t in case d of
    0 -> "List(" ++ (showType t) ++ ")"
    1 -> "ListOfList(" ++ (showType bt) ++ ")"
    otherwise -> "NestedLists(" ++ (showType bt) ++ ", " ++ (show $ d-2) ++ ")"
showType (Struct name) = name
showType t = show t

isPrimitveType :: Type -> Bool
isPrimitveType PrimVoid    = True
isPrimitveType PrimBool    = True
isPrimitveType PrimUInt8   = True
isPrimitveType PrimUInt16  = True
isPrimitveType PrimUInt32  = True
isPrimitveType PrimUInt64  = True
isPrimitveType PrimInt8    = True
isPrimitveType PrimInt16   = True
isPrimitveType PrimInt32   = True
isPrimitveType PrimInt64   = True
isPrimitveType PrimFloat32 = True
isPrimitveType PrimFloat64 = True
isPrimitveType Enum        = True
isPrimitveType _ = False

isPointerType :: Type -> Bool
isPointerType Text = True
isPointerType Data = True
isPointerType (List _) = True
isPointerType (Struct _) = True
isPointerType Interface = True
isPointerType AnyPtr = True
isPointerType _ = False

isStructType :: Type -> Bool
isStructType (Struct _) = True
isStructType _ = False

isListOrBlobType :: Type -> Bool
isListOrBlobType (List _) = True
isListOrBlobType Text = True
isListOrBlobType Data = True
isListOrBlobType _ = False

--                  prim  str   ptr   lblb (yes I love this short name for ListOrBlob)
type TypeSummary = (Bool, Bool, Bool, Bool)

summarizeType :: Type -> TypeSummary
summarizeType t = (isPrimitveType t, isStructType t, isPointerType t, isListOrBlobType t)


emitFile :: String -> String -> [Node] -> IO ()
emitFile indent name nested = mapM_ (emitNode indent) nested


emitSlotGetFunction :: String -> String -> Maybe Int -> Type -> TypeSummary -> IO ()
---------------------------------------- prim   str    ptr    lblb
emitSlotGetFunction indent name discr t (True , False, False, False) = putStrLn $ indent ++ "fn get_" ++ name ++ "(buffer, address) -> t"
emitSlotGetFunction indent name discr t (False, _    , True , _    ) = putStrLn $ indent ++ "fn get_" ++ name ++ "(buffer, address) -> t.Stencil"
emitSlotGetFunction indent name discr t _ = putStrLn $ indent ++ "// missing get case"


emitSlotSetFunction :: String -> String -> Maybe Int -> Type -> TypeSummary -> IO ()
---------------------------------------- prim   str    ptr    lblb
emitSlotSetFunction indent name discr t (True , False, False, False) = putStrLn $ indent ++ "fn set_" ++ name ++ "(buffer, address, t)" ++ (showDiscriminantChange discr)
emitSlotSetFunction indent name discr t (False, _    , True , _    ) = putStrLn $ indent ++ "fn set_" ++ name ++ "(buffer, address, t.Stencil)" ++ (showDiscriminantChange discr)
emitSlotSetFunction indent name discr t _ = putStrLn $ indent ++ "// missing set case"


emitSlotInitFunction :: String -> String -> Maybe Int -> Type -> TypeSummary -> IO ()
----------------------------------------- prim   str    ptr    lblb
emitSlotInitFunction indent name discr t (False, False, True , True ) = putStrLn $ indent ++ "fn init_" ++ name ++ "(buffer, n) -> " ++ (showType t) ++ (showDiscriminantChange discr)
emitSlotInitFunction indent name discr t (False, True , True , False) = putStrLn $ indent ++ "fn init_" ++ name ++ "(buffer) -> " ++ (showType t) ++ (showDiscriminantChange discr)
emitSlotInitFunction _ _ _ _ _ = return ()
 

emitSlotHasFunction :: String -> String -> Maybe Int -> Type -> TypeSummary -> IO ()
---------------------------------------- prim   str    ptr    lblb
emitSlotHasFunction indent name discr t (False, _    , True , _     ) = putStrLn $ indent ++ "fn has_" ++ name ++ "(buffer) -> Bool"
emitSlotHasFunction _ _ _ _ _ = return ()


emitSlotAdoptFunction :: String -> String -> Maybe Int -> Type -> TypeSummary -> IO ()
------------------------------------------ prim   str    ptr    lblb
emitSlotAdoptFunction indent name discr t (False, _    , True , _     ) = putStrLn $ indent ++ "fn adopt_" ++ name ++ "(buffer, address)" ++ (showDiscriminantChange discr)
emitSlotAdoptFunction _ _ _ _ _ = return ()


emitSlotDisownFunction :: String -> String -> Maybe Int -> Type -> TypeSummary -> IO ()
------------------------------------------- prim   str    ptr    lblb
emitSlotDisownFunction indent name discr t (False, _    , True , _     ) = putStrLn $ indent ++ "fn disown_" ++ name ++ "(buffer) -> address"
emitSlotDisownFunction _ _ _ _ _ = return ()


emitSlotFunctions :: String -> Field -> IO ()
emitSlotFunctions indent (Slot name t discr) = let s = (summarizeType t) in do
    emitSlotGetFunction    indent name discr t s
    emitSlotSetFunction    indent name discr t s
    emitSlotHasFunction    indent name discr t s
    emitSlotInitFunction   indent name discr t s
    emitSlotAdoptFunction  indent name discr t s
    emitSlotDisownFunction indent name discr t s


emitGroupFunctions :: String -> String -> [Field] -> Int -> IO ()
emitGroupFunctions indent name fields discr = do
    mapM_ (emitSlotFunctions indent) fields
    if (discr == 0) then return () else putStrLn $ indent ++ "fn which(buffer) -> discr"


emitGroup :: String -> String -> [Node] -> [Field] -> Int -> IO ()
emitGroup indent name [] fields discr = do
    putStrLn $ indent ++ "struct " ++ name ++ " {"
    emitGroupFunctions (indent ++ "    ") name fields discr
    putStrLn $ indent ++ "}"


emitStructFunctions :: String -> String -> [Field] -> Int -> IO ()
emitStructFunctions indent name fields discr = do
    putStrLn $ indent ++ "fn init_from(buffer, address) -> " ++ name
    putStrLn $ indent ++ "fn init_as_root(buffer) -> " ++ name
    emitGroupFunctions indent name fields discr


emitStruct :: String -> String -> [Node] -> [Field] -> Int -> IO ()
emitStruct indent name nested fields discr = do
    putStrLn $ indent ++ "struct " ++ name ++ " {"
    putStrLn $ indent ++ "    |..... subs ..........|"
    mapM_ (emitNode  (indent ++ "    ")) nested
    putStrLn $ indent ++ "    |..... functions .....|"
    emitStructFunctions (indent ++ "    ") name fields discr
    putStrLn $ indent ++ "}"


emitNode :: String -> Node -> IO ()
emitNode indent (FileNode name nested) = emitFile indent name nested
emitNode indent (StructNode name nested fields False discr) = emitStruct indent name nested fields discr
emitNode indent (StructNode name nested fields True discr) = emitGroup indent name nested fields discr
emitNode indent _ = putStrLn $ indent ++ "Unsupported node"


-- struct Dudule {
--     struct Inner {
--         e @0 :SomeEnum ;
--     }
--     a @0 :UInt32 ;
--     b @1 :UInt32 ;
--     bs @2 :List(List(List(Float64))) ;
--     inners @3 :List(List(Inner)) ;
-- }
fakeSchema :: Node
fakeSchema = (FileNode "truc.capnp"
    [StructNode "Dudule"
        [StructNode "Inner" []
            [
                (Slot "e" Enum Nothing)
            ]
            False 0]
        [
            (Slot "a" PrimUInt32 Nothing),
            (Slot "b" PrimUInt32 Nothing),
            (Slot "bs" (List (List (List PrimFloat64))) Nothing),
            (Slot "inners" (List (List (Struct "Inner"))) Nothing)
        ]
        False 0])

-- -- struct Machin {
-- --     union {
-- --         xs @0 :List(UInt32) ;
-- --         ys @1 :group {
-- --             a @2 :Float32 ;
-- --             b @3 :Float64 ;
-- --         }
-- --     }
-- -- }
-- fakeSchema :: Node
-- fakeSchema = (FileNode "truc.capnp"
--     [StructNode "Machin"
--         [StructNode "ys" []
--             [
--                 (Slot "a" PrimFloat32 Nothing),
--                 (Slot "b" PrimFloat64 Nothing)
--             ]
--             True 0]
--         [
--             (Slot "xs" (List PrimUInt32) (Just 0)),
--             (Slot "ys" (Struct "ys") (Just 1))
--         ]
--         False 2])


main = do
    emitNode "" fakeSchema
