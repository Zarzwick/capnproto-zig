# Cap'n'proto generator for zig

This is a code generator emitting zig code to serialize/deserialize things following the schema-based protocol cap'n'proto.
This is experimental, and for now RPC is not planned (as in "I haven't thought about it for the code architecture").

## Development

*For now all of the development happens on a fossil repository I have, somewhere on a raspberry pi. If you want to contribute, I may simply resort to use only gitlab, for simplicity.*

Anyway, right now everything is controlled by the `Makefile`. You can:

- `make plugin` to build the C++ plugin
- `make generation` to generate files required for testing
- `make tests` to actually run the tests
- `make` to run everything above
- `make poc` to run a simplified haskell proof-of-concept that may be useful to understand the plugin call-graph.

## Missing

A **lot** of things are missing:

- The whole reading part, actually. This is because it will be very similar to the writing part, and I'd rather finish this one first.
- Proper integration of the unpacked compression in the compressor.
- Everything around Lists has yet to be assembled correctly.
- adopt/disown functions.

A **good place to start** is `tests/tests.zig`, which demonstrates how (and to which extent) one is expected to use the generated code.


<!--
At some Differences with

.. + Apache Parquet
.. + Flatbuffers
.. + Protobuf
-->
